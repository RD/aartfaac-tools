#ifndef BASELINE_UTILS_H_
#define BASELINE_UTILS_H_

#include <stdint.h>
#include <utility>

/*
 * Computes the number of baselines, including auto-correlations.
 */
inline unsigned int compute_nr_baselines(unsigned int n_antennas) {
  return n_antennas * (n_antennas - 1) / 2 + n_antennas;
}

/*
 * Computes the baseline index corresponding to antenna a and b, given the
 * number of antennas. It assumes that auto-correlations of antennas are
 * included.
 */
inline unsigned int compute_baseline_index(unsigned int a, unsigned int b,
                                           unsigned int nr_antennas) {
  return (a * nr_antennas) + b - a * (a + 1) / 2;
}

/*
 * Computes the antenna pair (baseline) given the baseline index. It assumes
 * that auto-correlations of antennas are included.
 */
inline std::pair<unsigned int, unsigned int> compute_baseline(
    unsigned int i, unsigned int nr_antennas) {
  // Count baselines backwards
  unsigned int n_baselines = compute_nr_baselines(nr_antennas);
  unsigned int j = n_baselines - i;

  // Compute antenna 1
  unsigned int a = int(nr_antennas - (-1 + sqrtf(8 * j)) / 2);

  // Compute the number of baselines (a, b) with a < antenna1
  unsigned int n = ((nr_antennas * 2 + 1) * a - (a * a)) / 2;

  // Compute antenna 2
  unsigned int b = i - n + a;

  // Return baseline (a, b)
  return std::pair<unsigned int, unsigned int>(a, b);
}

/*
 * Computes the list of baselines for a given antenna. It assumes that
 * auto-correlations are included.
 */
inline std::vector<unsigned int> compute_baseline_list(
    unsigned int antenna, unsigned int nr_antennas) {
  std::vector<unsigned int> indices(nr_antennas);

  for (unsigned int i = antenna; i < nr_antennas; i++) {
    // Compute the number of baselines up to antenna i
    unsigned int m = int((i * (i + 1)) / 2);

    if (i == antenna) {
      // Add all baselines (j, antenna)
      for (unsigned int j = 0; j < antenna + 1; j++) {
        indices[j] = m + j;
      }
    } else {
      // Add baseline (antenna, i)
      indices[i] = m + antenna;
    }
  }

  return indices;
}

#endif  // BASELINE_UTILS_H_
