#ifndef AARTFAAC_FILE_H
#define AARTFAAC_FILE_H

#include "aartfaacheader.h"
#include "aartfaacmode.h"
#include "aartfaacinput.h"
#include "aartfaacbuffer.h"

#include <complex>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <stdexcept>

// An AARTFAAC file has a header followed by the data, which is written as:
// std::complex<float>
// visibilities[nr_baselines][nr_channels][nr_pols][nr_pols];
class AartfaacFile : public AartfaacInput {
 public:
  AartfaacFile(const char *filename, AartfaacMode mode)
      : AartfaacInput(filename, mode) {
    _file.open(filename);

    _file.seekg(0, std::ios::end);
    _filesize = _file.tellg();

    // Read first header
    _file.seekg(0, std::ios::beg);
    _file.read(reinterpret_cast<char *>(&_header), sizeof(AartfaacHeader));

    _header.Check();

    _blockSize = sizeof(std::complex<float>) * _header.VisPerTimestep();
    _bandwidth = mode.Bandwidth();
    _frequency = _header.FirstChannelFrequency();

    SeekToTimestep(0);
  }

  AartfaacFile(const char *filename) : AartfaacInput(filename) {
    _file.seekg(0, std::ios::end);
    _filesize = _file.tellg();

    // Read first header
    _file.seekg(0, std::ios::beg);
    _file.read(reinterpret_cast<char *>(&_header), sizeof(AartfaacHeader));

    _header.Check();

    _blockSize = sizeof(std::complex<float>) * _header.VisPerTimestep();

    SeekToTimestep(0);
  }

  void SkipTimesteps(int count) override {
    _file.seekg(count * (sizeof(AartfaacHeader) + _blockSize), std::ios::cur);
    _blockPos += count;
  }

  void SeekToTimestep(size_t timestep) override {
    _file.seekg(timestep * (sizeof(AartfaacHeader) + _blockSize),
                std::ios::beg);
    _blockPos = timestep;
  }

  Timestep ReadTimestep(std::complex<float> *buffer) {
    _file.read(reinterpret_cast<char *>(&_header), sizeof(AartfaacHeader));
    _file.read(reinterpret_cast<char *>(buffer), _blockSize);
    if (!_file) throw std::runtime_error("Error reading file");
    ++_blockPos;

    return Timestep{TimeToCasa(_header.StartTime()),
                    TimeToCasa(_header.EndTime())};
  }

  Timestep ReadMetadata() override {
    AartfaacHeader h;
    _file.read(reinterpret_cast<char *>(&h), sizeof(AartfaacHeader));
    if (!_file) throw std::runtime_error("Error reading file");
    SeekToTimestep(_blockPos);

    return Timestep{TimeToCasa(h.StartTime()), TimeToCasa(h.EndTime())};
  }

  bool HasMore() const override { return _blockPos < (_filesize / _blockSize); }

  size_t NTimesteps() const override { return _filesize / _blockSize; }

  /**
   * CASA times are in MJD, but in s.
   */
  static double TimeToCasa(double timestamp) {
    return timestamp + ((2440587.5 - 2400000.5) * 86400.0);
  }

 private:
  std::ifstream _file;
  size_t _blockSize, _filesize;
};

#endif
