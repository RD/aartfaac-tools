#include "aartfaacfile.h"
#include "aartfaacbuffer.h"
#include "filedescriptors.h"
#include <regex>

/// Splits a string on the delimiter, returning a vector of the split strings.
/// \param str the input string to be split
/// \param delimiter the delimiter on which to split
/// \return the vector of split strings
std::vector<std::string> split(const std::string& str,
                               const std::string& delimiter) {
  std::vector<std::string> result;

  size_t searchPointer = 0;
  while (searchPointer < str.size()) {
    size_t newSearchPointer = str.find(delimiter, searchPointer);
    newSearchPointer = std::min(str.size(), newSearchPointer);
    result.push_back(
        str.substr(searchPointer, newSearchPointer - searchPointer));
    searchPointer = newSearchPointer + 1;
  }

  return result;
}

/// Parses an input string into a vector of AartfaacInputs, which can be read.
/// The input must be a comma-separated list of 'mode:name' tuples.
/// A mode can be either of
///     - file   an input file
///     - pipe   a named pipe
/// \param input the input string
/// \param mode the aartfaac mode with which to initialize the
/// AartfaacInput-type classes
/// \return The vector of initialized AartfaacInputs
std::vector<AartfaacInput> parse_file_descriptors(std::string& input,
                                                  AartfaacMode mode) {
  std::vector<std::string> descriptors = split(input, ",");
  std::vector<AartfaacInput> output;

  for (std::string& descriptor : descriptors) {
    std::vector<std::string> fp = split(descriptor, ":");
    if (fp.size() != 2) {
      std::cerr << "Warning: Skipping file descriptor '" << descriptor
                << "' as it cannot be parsed correctly." << std::endl;
      continue;
    }

    std::cout << fp[0] << " for " << fp[1] << std::endl;

    std::string& fd = fp[0];
    std::string& name = fp[1];

    if (fd == "file") output.push_back(AartfaacFile(name.c_str(), mode));
    if (fd == "buffer")
      throw std::logic_error(
          "Cannot parse buffer type as it needs input buffer.");
  }

  return output;
}
