#ifndef AARTFAAC_HEADER_H
#define AARTFAAC_HEADER_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <string>
#include <iostream>
#include <iomanip>

// Magic number in raw corr visibilities.
static const uint32_t HDR_MAGIC = 0x3B98F003;

namespace {
void ThrowMagicError() {
  throw std::runtime_error(
      "This file does not start with the standard header prefix. It is not "
      "a supported Aartfaac correlation file or is damaged.");
}

void ThrowCorrelationModeError(uint8_t correlationMode) {
  std::ostringstream str;
  str << "This Aartfaac file specifes a correlation mode of '"
      << int(correlationMode)
      << "'. This tool can only handle sets with 4 polarizations (mode "
         "15).";
  throw std::runtime_error(str.str());
}

template <typename T>
void PrintArray(std::ostringstream& str, T* data, size_t n) {
  T previous = data[0];
  int count = 1;

  for (size_t i = 1; i < n; ++i) {
    if (data[i] == previous) {
      ++count;
    } else {
      str << previous;
      if (count > 1) {
        str << "x" << count;
      }
      str << ", ";

      previous = data[i];
      count = 1;
    }
  }

  str << previous;
  if (count > 1) {
    str << "x" << count;
  }
}
}  // namespace

struct AartfaacHeader {
  uint32_t magic;
  uint16_t nrReceivers;
  uint8_t nrPolarizations;
  uint8_t correlationMode;
  double startTime, endTime;
  uint32_t weights[300];  // Fixed-sized field, independent of #stations!
  uint32_t nrSamplesPerIntegration;
  uint16_t nrChannels;
  char pad0[2];
  double firstChannelFrequency, channelBandwidth;
  char pad1[288];

  AartfaacHeader()
      : magic(HDR_MAGIC),
        nrReceivers(0),
        nrPolarizations(0),
        correlationMode(0),
        startTime(0),
        endTime(0),
        nrSamplesPerIntegration(0),
        nrChannels(0),
        firstChannelFrequency(0),
        channelBandwidth(0) {
    memset(weights, 0, sizeof(weights));
    memset(pad0, 0, sizeof(pad0));
    memset(pad1, 0, sizeof(pad1));
  }

  uint32_t Magic() const { return magic; };
  double NrReceivers() const { return nrReceivers; };
  double NrPolarizations() const { return nrPolarizations; };
  double CorrelationMode() const { return correlationMode; };
  double StartTime() const { return startTime; };
  double EndTime() const { return endTime; };
  size_t NrWeights() const { return 300; };
  uint32_t* Weights() { return weights; };
  uint16_t NrSamplesPerIntegration() const { return nrSamplesPerIntegration; };
  uint16_t NrChannels() const { return nrChannels; };
  double FirstChannelFrequency() const { return firstChannelFrequency; }
  double ChannelBandwidth() const { return channelBandwidth; }

  size_t VisPerTimestep() const {
    size_t nBaselines = NrReceivers() * (NrReceivers() + 1) / 2;
    return nBaselines * NrChannels() * NrPolarizations();
  }

  void Check() {
    if (magic != Magic()) {
      ThrowMagicError();
    }
    if (correlationMode != 15) {
      ThrowCorrelationModeError(correlationMode);
    }
  }

  std::string ToString() {
    std::ostringstream str;
    str << "magic = " << Magic() << '\n';
    str << "nrReceivers = " << NrReceivers() << '\n'
        << "nrPolarizations = " << unsigned(NrPolarizations()) << '\n'
        << "correlationMode = " << unsigned(CorrelationMode()) << '\n'
        << std::fixed << "startTime = " << StartTime() << '\n'
        << "endTime = " << EndTime() << " (total: " << (EndTime() - StartTime())
        << " s)\n"
        << "weights = [";  //
    PrintArray(str, Weights(), NrWeights());
    str << "] (" << NrWeights() << "x)\n";
    str << "nrSamplesPerIntegration = " << NrSamplesPerIntegration() << '\n'
        << "nrChannels = " << NrChannels() << '\n';
    str << std::fixed << std::setprecision(3)
        << "firstChannelFrequency = " << firstChannelFrequency / 1e6 << " MHz\n"
        << "channelBandwidth = " << channelBandwidth / 1e6 << " MHz\n";
    return str.str();
  }
};

static_assert(sizeof(AartfaacHeader) == 1536,
              "Header should be of size 1536 bytes");

#endif
