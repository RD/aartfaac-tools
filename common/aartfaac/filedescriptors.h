#ifndef AARTFAAC2MS_FILEDESCRIPTORS_H
#define AARTFAAC2MS_FILEDESCRIPTORS_H

#include <vector>
#include <string>
#include "aartfaacinput.h"
#include "aartfaacmode.h"

std::vector<AartfaacInput> parse_file_descriptors(std::string& input,
                                                  AartfaacMode mode);
std::vector<std::string> split(const std::string& str,
                               const std::string& delimiter);
#endif  // AARTFAAC2MS_FILEDESCRIPTORS_H
