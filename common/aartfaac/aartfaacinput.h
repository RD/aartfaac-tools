#ifndef AARTFAAC2MS_AARTFAACINPUT_H
#define AARTFAAC2MS_AARTFAACINPUT_H

#include <complex>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include "aartfaacmode.h"
#include "aartfaacheader.h"

struct Timestep {
  double startTime, endTime;
};

// An AARTFAAC file has a header followed by the data, which is written as:
// std::complex<float>
// visibilities[nr_baselines][nr_channels][nr_pols][nr_pols];
class AartfaacInput {
 public:
  AartfaacInput(const char *filename, AartfaacMode mode)
      : _filename(filename), _mode(mode) {}

  AartfaacInput(const char *filename)
      : _filename(filename), _mode(AartfaacMode::Unused) {}

  virtual void SkipTimesteps(int count) {}

  virtual void SeekToTimestep(size_t timestep) {}

  virtual Timestep ReadTimestep(std::complex<float> *buffer) { return {0, 0}; }

  virtual Timestep ReadMetadata() { return {0, 0}; }

  virtual bool HasMore() const { return false; }

  virtual size_t NTimesteps() const { return 0; }

  size_t VisPerTimestep() const { return _header.VisPerTimestep(); }

  size_t NChannels() const { return _header.NrChannels(); }
  size_t NAntennas() const { return _header.NrReceivers(); }
  size_t NPolarizations() const { return _header.NrPolarizations(); }
  uint8_t CorrelationMode() const { return _header.CorrelationMode(); }

  double Bandwidth() const { return _bandwidth; }
  double StartTime() const { return TimeToCasa(_header.StartTime()); }
  double Frequency() const { return _frequency; }
  double IntegrationTime() const {
    return _header.EndTime() - _header.StartTime();
  }
  uint BlockPos() const { return _blockPos; }
  /**
   * CASA times are in MJD, but in s.
   */
  static double TimeToCasa(double timestamp) {
    return timestamp + ((2440587.5 - 2400000.5) * 86400.0);
  }

 protected:
  uint _blockPos = 0;
  std::string _filename;
  AartfaacHeader _header;
  AartfaacMode _mode;
  double _frequency, _bandwidth;
};

#endif  // AARTFAAC2MS_AARTFAACINPUT_H
