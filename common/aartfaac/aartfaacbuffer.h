#ifndef AARTFAAC_STREAM_H
#define AARTFAAC_STREAM_H

#include "aartfaacheader.h"
#include "aartfaacmode.h"

#include <algorithm>
#include <complex>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdexcept>
#include <sys/stat.h>

#include <fcntl.h>

// An AARTFAAC file has a header followed by the data, which is written as:
// std::complex<float>
// visibilities[nr_baselines][nr_channels][nr_pols][nr_pols];
class AartfaacBuffer : public AartfaacInput {
 public:
  AartfaacBuffer(AartfaacHeader header,
                 std::vector<std::complex<float>> *buffer, AartfaacMode mode)
      : AartfaacInput("", mode), _isMetadataRead(false) {
    this->_buffer = buffer;

    // Read first header
    this->_header = header;
    _isMetadataRead = true;
    header.Check();

    _blockSize = sizeof(std::complex<float>) * header.VisPerTimestep();

    std::cout << "Calculating frequency for " << _sbIndex << std::endl;

    // This is from:
    // http://astron.nl/radio-observatory/astronomers/users/
    //   technical-information/frequency-selection/station-clocks-and-rcu
    _bandwidth = mode.Bandwidth();
    const double frequencyOffset = mode.FrequencyOffset();
    _frequency = _bandwidth * double(_sbIndex) + frequencyOffset;
  }

  void SkipTimesteps(int count) override {
    throw std::runtime_error(
        "Cannot skip timesteps in AartfaacBuffer input type.");
  }

  void SeekToTimestep(size_t timestep) override {
    if (timestep == 0) return;
    throw std::runtime_error(
        "Cannot seek to timestep when using AartfaacBuffer, set --interval "
        "start to 0");
  }

  Timestep ReadTimestep(std::complex<float> *outBuffer) override {
    ReadMetadata();

    std::copy_n(reinterpret_cast<char *>(_buffer->data()), uint(_blockSize),
                reinterpret_cast<char *>(outBuffer));
    _isMetadataRead = false;

    return Timestep{TimeToCasa(_header.StartTime()),
                    TimeToCasa(_header.EndTime())};
  }

  Timestep ReadMetadata() override {
    // Return earlier metadata if it is already read.
    if (_isMetadataRead)
      return Timestep{TimeToCasa(_header.StartTime()),
                      TimeToCasa(_header.EndTime())};

    throw std::logic_error(
        "One-timestep AartfaacBuffer already initialized metadata.");
  }

  bool HasMore() const override { return true; }

  size_t NTimesteps() const override { return 1; }

  /**
   * CASA times are in MJD, but in s.
   */
  static double TimeToCasa(double timestamp) {
    return timestamp + ((2440587.5 - 2400000.5) * 86400.0);
  }

 private:
  std::vector<std::complex<float>> *_buffer;
  bool _isMetadataRead;
  size_t _blockSize, _sbIndex;
};

#endif
