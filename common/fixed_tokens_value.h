#ifndef _FIXED_TOKENS_VALUE_H_
#define _FIXED_TOKENS_VALUE_H_

/*
 * Code altered from;
 * https://stackoverflow.com/questions/8175723/vector-arguments-in-boost-program-options
 */

#include "boost/program_options.hpp"

namespace po = boost::program_options;

template <typename T, typename charT = char>
class fixed_tokens_typed_value : public po::typed_value<T, charT> {
  typedef po::typed_value<T, charT> base;
  unsigned _min;
  unsigned _max;

 public:
  fixed_tokens_typed_value(T *t, unsigned min, unsigned max)
      : base(t), _min(min), _max(max) {
    base::multitoken();
  }
  unsigned min_tokens() const { return _min; }

  unsigned max_tokens() const { return _max; }
};

template <typename T>
fixed_tokens_typed_value<T> *fixed_tokens_value(unsigned min, unsigned max) {
  return new fixed_tokens_typed_value<T>(0, min, max);
}

template <typename T>
fixed_tokens_typed_value<T> *fixed_tokens_value(T *t, unsigned min,
                                                unsigned max) {
  return new fixed_tokens_typed_value<T>(t, min, max);
}
#endif  //_FIXED_TOKENS_VALUE_H_