#ifndef PARAMETER_PROVIDER_H
#define PARAMETER_PROVIDER_H

#include <boost/program_options.hpp>
#include "aartfaac/aartfaacmode.h"

namespace po = boost::program_options;
class ParameterProviderArguments {
 public:
  // After calling parseArgs, this ParameterProviderArguments class should hold
  // all arguments.
  virtual po::options_description getDescription() = 0;
  virtual po::positional_options_description getPositional() = 0;
  virtual void parseArgs(const po::variables_map& vm) = 0;

 private:
  po::positional_options_description _positional;
  po::options_description _description;
};

class ParameterProvider {
 public:
  ParameterProvider(const std::string& header);

  void addArguments(ParameterProviderArguments& ppa);
  void parse(int argc, char* argv[]);

  po::variables_map getVariableMap();

 private:
  po::variables_map _vm;
  po::positional_options_description _positional;
  po::options_description _description;

  std::vector<ParameterProviderArguments*> _ppaRefs;
};

#endif  // PARAMETER_PROVIDER_H
