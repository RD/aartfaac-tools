#include <iostream>
#include "parameter_provider.h"
#include "version.h"

namespace po = boost::program_options;

void ParameterProvider::parse(int argc, char* argv[]) {
  // Parse the parameters, and display error information if necessary.
  po::store(po::command_line_parser(argc, argv)
                .options(_description)
                .positional(_positional)
                .run(),
            _vm);
  try {
    notify(_vm);
  } catch (std::exception& ex) {
    std::cerr << ex.what() << std::endl << std::endl;
    std::cerr << _description << std::endl;
    exit(1);
  }

  if (_vm.count("help")) {
    std::cout << _description << std::endl;
    exit(0);
  }

  // Pass the arguments to the linked objects.
  for (ParameterProviderArguments* ppa : _ppaRefs) {
    ppa->parseArgs(_vm);
  }
}

ParameterProvider::ParameterProvider(const std::string& header) {
  _description.add(po::options_description(header));
  _description.add_options()("help,h", "Produce this message.");
}

void ParameterProvider::addArguments(ParameterProviderArguments& ppa) {
  _description.add(ppa.getDescription());

  auto positional = ppa.getPositional();
  for (size_t i = 0; i < positional.max_total_count(); i++) {
    _positional.add(positional.name_for_position(i).c_str(), 1);
  }

  // Keep reference of all ppas, so we can do callbacks later.
  _ppaRefs.push_back(&ppa);
}
po::variables_map ParameterProvider::getVariableMap() { return _vm; }
