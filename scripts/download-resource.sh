#!/bin/bash

# Check command-line arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <filename>"
fi

# Create a directory for the resource
mkdir -p $DATA_DIR
cd $DATA_DIR

# Arguments
PREFIX=https://support.astron.nl/software/ci_data/aartfaac-tools/
FILENAME=$1

# The input filename should either be a normal string, or
# appended with e.g. .zip to denote that it is a compressed file

# Check whether the file or the extracted
# version thereof already exists
if [[ -f "${FILENAME}" || -f "${FILENAME%.*}" || -d "${FILENAME%.*}" ]]
then
    echo "${FILENAME} already exists"
    exit 0
fi

# Download the file
echo "Downloading ${FILENAME}"
wget -q ${PREFIX}/${FILENAME} -O ${FILENAME}

# Extract the file (if needed)
if [[ "${FILENAME}" == *.zip ]]
then
  echo "Extracting ${FILENAME}"
  unzip -q $FILENAME
  rm $FILENAME
fi