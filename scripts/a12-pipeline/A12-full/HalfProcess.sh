#!/bin/bash
N_CPUS=${2:-8}

export OPENBLAS_NUM_THREADS=1

MS=$1

# Flagging and calibration
echo "Processing file $MS"
time DP3 newcombined.parset \
	msin=$MS msin.datacolumn=DATA \
	numthreads=$N_CPUS \
	gaincal.sourcedb=A12_small.blob \
	gaincal.parmdb=$MS/instrument.h5 \
	ddecal2.sourcedb=A12_small.blob \
	ddecal2.h5parm=$MS/dde_instrument.h5 \
	msout.datacolumn=PROCESSED_DATA


# Imaging
echo "Processing file $MS" 
time ./image.sh $MS-$i $MS 

