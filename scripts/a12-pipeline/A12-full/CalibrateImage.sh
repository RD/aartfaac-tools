#!/bin/bash
export OPENBLAS_NUM_THREADS=1

N_CPUS=8
SOURCE_DB=/var/scratch/dkampert/padre/A12_small.blob
DMS=$1

ddeonly=true
if [ ddeonly ]; then
  parset=/var/scratch/dkampert/padre/parsets/ddecalcombined.parset
  time DP3 $parset \
	steps=[antflagger,ddecal1,applycal,ddecal2] \
	msin=$DMS msin.datacolumn=DATA \
	numthreads=$N_CPUS \
	applycal.parmdb= \
        ddecal1.sourcedb=$SOURCE_DB \
	ddecal1.h5parm=$DMS/instrument.h5 \
	ddecal2.sourcedb=$SOURCE_DB \
	ddecal2.h5parm=$DMS/dde_instrument.h5 \
	msout.datacolumn=PROCESSED_DATA

else
  parset=/var/scratch/dkampert/padre/parsets/newcombined.parset

  # Calibration
  echo "Processing file $DMS"
  time DP3 $parset \
      msin=$DMS msin.datacolumn=DATA \
      numthreads=$N_CPUS \
      gaincal.sourcedb=$SOURCE_DB \
      gaincal.h5parm=$DMS/instrument.h5 \
      ddecal2.sourcedb=$SOURCE_DB \
      ddecal2.h5parm=$DMS/dde_instrument.h5 \
      msout.datacolumn=PROCESSED_DATA
fi

# Imaging
echo "Processing file $DMS" 
IMG_NAME=$(basename $DMS)
time ./image.sh ${OUTPUT_DIR}/$IMG_NAME $DMS

