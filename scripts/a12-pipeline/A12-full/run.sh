#!/bin/bash
# Define executables
AARTFAAC_EXE=${AARTFAAC_ROOT_DIR}/AARTFAAC

TEMP=${TEMP_DIR}/ms_output
ANTENNACONF=${DATA_DIR}/A12-AntennaField.conf

mkdir -p $TEMP
mkdir -p ${OUTPUT_DIR}

FILENAMES="file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st00","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st01","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st02","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st03","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st04","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st05","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st06","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st07","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st08","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st09","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st10","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st11"
SUBBANDS=166,167,185,186,194,195,320,321

# Setup pipes
PIPES=""
for pipe in 00 01 02 03 04 05 06 07; do
  PIPES=$PIPES"pipe:$pipe,"
  OUTPUT_PIPES=$OUTPUT_PIPES"$pipe-0,"
done
PIPES=${PIPES::-1}
OUTPUT_PIPES=${OUTPUT_PIPES::-1}
 
# Start correlator
TZ=UTC $AARTFAAC_EXE -p1 -n576 -t768 -c256 -C8 -b16 -s8 -m15 -D "2021-08-10 09:02:12" -r4 -g0 -q1 -R0 -S166,167,185,186,194,195,320,321 -i "file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st00","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st01","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st02","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st03","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st04","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st05","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st06","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st07","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st08","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st09","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st10","file:/var/scratch/romein/AARTFAAC-raw/Tue Aug 10 09:02:12 UTC 2021/raw-sb00-st11" -o $PIPES >> log/correlator.out 2>&1 &

# Setup converter processes
stream-aggregator "$OUTPUT_PIPES" $TEMP $ANTENNACONF --packetloss_threshold=20 --mode 1

