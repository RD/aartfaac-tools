#!/bin/bash
NTHREADS=8
IMAGE_NAME="${1:-iteration}"
NITER=0
CLEAN=false
COLUMN=PROCESSED_DATA

if $CLEAN; then
  OPENBLAS_NUM_THREADS=1 wsclean -size 2300 2300 -scale 0.05 -no-update-model-required -pol I \
   	-niter 10000 -name "$IMAGE_NAME" -j $NTHREADS -no-dirty -data-column \
	$COLUMN "${@:2}"
else
  OPENBLAS_NUM_THREADS=1 wsclean -size 2300 2300 -scale 0.05 -j $NTHREADS -parallel-reordering $NTHREADS \
		-no-update-model-required -pol I -weight briggs 0.0 \
                -name "$IMAGE_NAME" -niter $NITER -auto-mask 3 -auto-threshold 0.3 \
                -local-rms -mgain 0.8 -clean-border 21  -multiscale -nmiter 20 \
                -data-column $COLUMN "${@:2}";
fi

