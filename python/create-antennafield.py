#!/usr/bin/env python3

import argparse
import numpy as np

from datetime import datetime

import lofarantpos.db


def print_vector(output, vector):
    data = np.array2string(
        vector,
        precision=6,
        separator="   ",
        formatter={"float_kind": lambda x: "%.6f" % x},
    )
    data = data.replace("[", "[   ").replace("]", " ]")
    output.write(f"{len(vector)} {data}\n")


def print_phase_reference(output, vector):
    data = "{:} [ {:.9f} {:.9f} {:.3f} ]".format(len(vector), *vector)
    output.write(f"{data}\n")


def print_rotation_matrix(output, name, rotation_matrix):
    # normal vector
    output.write(f"NORMAL_VECTOR {name}\n")

    normal_vector = rotation_matrix[:, 2]
    print_vector(output, normal_vector)
    output.write("\n")

    # rotation matrix
    prefix = " x ".join(np.asarray(rotation_matrix.shape, dtype=str))
    output.write(f"ROTATION_MATRIX {name}\n{prefix} [\n")
    for row in np.round(rotation_matrix, 6):
        row = np.array2string(
            row,
            separator="  ",
            prefix="",
            formatter={"float_kind": lambda x: "%13.10f" % x},
        )
        row = row.replace("[", "").replace("]", "")
        output.write(f" {row} \n")
    output.write("]\n\n")


def print_antenna_positions(output, name, phase_centre_reference, positions_lba):
    output.write(f"{name}\n")

    # phase centre reference
    print_phase_reference(output, phase_centre_reference)

    # positions
    prefix = " x ".join(
        np.asarray((positions_lba.shape[0], 2, positions_lba.shape[1]), dtype=str)
    )
    data = positions_lba
    output.write(f"{prefix} [\n")

    for row in data:
        row = np.round(row, 3)
        row = np.array2string(
            row, separator="", formatter={"float_kind": lambda x: "%11.05f" % x}
        )
        row = row.strip("[]")
        output.write(f"{row}   {row}\n")
    output.write("]\n\n")


def compute_antenna_positions_lba(phase_centre_reference, stations):
    n_antennas_per_station = 48
    n_antennas = n_antennas_per_station * len(stations)

    x = []
    y = []
    z = []
    for station in stations:
        phase_centre = db.antenna_etrs(station)[48:]
        if len(phase_centre) == 0:
            raise RuntimeError(f"{station} is not a valid station name.")
        station_etrs = phase_centre - phase_centre_reference
        station_etrs = station_etrs.reshape((len(station_etrs / 3), 3))
        x.append(station_etrs[:, 0])
        y.append(station_etrs[:, 1])
        z.append(station_etrs[:, 2])

    x = np.asarray(x)
    y = np.asarray(y)
    z = np.asarray(z)

    positions = np.zeros((n_antennas, 3))
    positions[:, 0] = x.flatten()
    positions[:, 1] = y.flatten()
    positions[:, 2] = z.flatten()

    return positions


def compute_antenna_positions_hba(stations):
    n_antennas_per_station = 48
    n_antennas = n_antennas_per_station * len(stations)
    return np.zeros((n_antennas, 3))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create antennafield configuration file."
    )
    parser.add_argument(
        "-s",
        "--stations",
        nargs="+",
        required=True,
        help="List of stations, e.g. 'CS001 CS002 CS003 CS004 CS005 CS006'",
    )
    parser.add_argument(
        "-o",
        "--output",
        required=True,
        help="Name of output file, e.g. 'AntennaField.conf'",
    )
    args = parser.parse_args()

    # Create station lists
    stations_lba = [station + "LBA" for station in args.stations]
    stations_hba = [station + "HBA" for station in args.stations]

    # Open output file
    output = open(args.output, "w")

    # Write header
    output.write(
        f"""#
# AntennaPositions for AARTFAAC-12 LBA_OUTER antennas
# ITRF2005 target_date = 2015.5
# Created: {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
#\n\n"""
    )

    # Open antenna database
    db = lofarantpos.db.LofarAntennaDatabase()

    # Write LBA section
    rotation_matrix_lba = db.pqr_to_etrs["CS002LBA"]  # CS002 is the reference station
    phase_centre_reference_lba = db.phase_centres["CS002LBA"]
    positions_lba = compute_antenna_positions_lba(
        phase_centre_reference_lba, stations_lba
    )
    print_rotation_matrix(output, "LBA", rotation_matrix_lba)
    print_antenna_positions(output, "LBA", phase_centre_reference_lba, positions_lba)

    # Write HBA section
    rotation_matrix_hba0 = db.pqr_to_etrs["CS002HBA0"]
    rotation_matrix_hba1 = db.pqr_to_etrs["CS002HBA1"]
    phase_centre_reference_hba0 = db.phase_centres["CS002HBA0"]
    phase_centre_reference_hba1 = db.phase_centres["CS002HBA1"]
    positions_hba = compute_antenna_positions_hba(stations_hba)
    print_antenna_positions(output, "HBA", np.zeros(3), positions_hba)
    print_rotation_matrix(output, "HBA0", rotation_matrix_hba0)
    output.write("HBA0\n")
    print_phase_reference(output, np.zeros(3))
    output.write("\n")
    print_rotation_matrix(output, "HBA1", rotation_matrix_hba1)
    output.write("HBA1\n")
    print_phase_reference(output, np.zeros(3))
    output.close()
