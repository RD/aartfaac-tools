#!/usr/bin/env python3

import argparse
import logging

from aartfaac_header import *
from aartfaac_helper import *

if __name__ == "__main__":
    logger = logging.getLogger()
    logging.basicConfig(format="[%(levelname)s] %(message)s", level=logging.DEBUG)

    parser = argparse.ArgumentParser(description="Read and process data with header.")
    parser.add_argument(
        "first", help="Path to the first input data file"
    )
    parser.add_argument(
        "second", help="Path to the second input data file"
    )
    args = parser.parse_args()

    file_first = open(args.first, "rb")
    file_second = open(args.second, "rb")

    header_first = Header(logger, args.first, file_first)
    header_second = Header(logger, args.second, file_second)

    print(f">>> First header: ")
    print(header_first.to_string())
    print()

    print(f">>> Second header: ")
    print(header_second.to_string())
    print()

    header_diff = header_first.diff(header_second)
    print(f">>> Difference: ")
    print(header_diff.to_string())
    print()
