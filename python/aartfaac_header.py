from copy import deepcopy
import itertools
import math
import re
import struct

from aartfaac_helper import *


def print_list(data):
    result = list()
    for key, group in itertools.groupby(data):
        group_list = list(group)
        if len(group_list) > 1:
            result.append(f"{len(group_list)}x{key}")
        else:
            for item in group_list:
                result.append(item)
    print(f"[{', '.join(result)}] ({len(data)}x)")


def list_to_string(data):
    result = list()
    for key, group in itertools.groupby(data):
        group_list = list(group)
        if len(group_list) > 1:
            result.append(f"{len(group_list)}x{key}")
        else:
            for item in group_list:
                result.append(str(item))
    return f"[{', '.join(result)}] ({len(data)}x)"


def compute_order(x):
    if x == 0:
        return 0
    return math.floor(math.log10(abs(x)))


class Header:
    def __init__(self, logger, filename, file):
        self.logger = logger
        self.filename = filename
        position = file.tell()

        if self.read_v2(file):
            self.logger.info("Header was read in v2 format")
            return

        file.seek(position)
        if self.read_v1(file):
            self.logger.info("Header was read in v1 format")
            return

        raise RuntimeError("Failed to read header")

    def extract_subband(self, text):
        # First try to find subband number with sb or SB prefix
        pattern = r"(sb|SB)(\d{3})"
        match = re.search(pattern, text)
        if match:
            return int(match.group(2))

        # Alternatively, look for any three-digit number
        pattern = r"\d{3}"
        match = re.search(pattern, text)
        if match:
            return int(match.group(0))

        # Not found
        return 0

    def get_header_format_v1(self):
        return "I H B B d d 78I I H H d d 152x"

    def get_header_format_v2(self):
        return "I H B B d d 300I I H 2x d d 288x"

    def read_v1(self, file):
        header_format = self.get_header_format_v1()
        header_size = struct.calcsize(header_format)
        assert header_size == 512

        header_data = file.read(header_size)
        position = file.tell()

        if len(header_data) < header_size:
            file.seek(position)
            return False

        header = struct.unpack(header_format, header_data)
        (
            self.magic,
            self.n_receivers,
            self.n_correlations,
            self.correlation_mode,
            self.start_time,
            self.end_time,
            *weights,
            self.n_samples_per_integration,
            self.n_channels,
            self.subband,
            self.first_channel_frequency,
            self.channel_bandwidth,
        ) = header
        self.weights = 300 * [0]
        self.weights[:len(weights)] = [int(weight) for weight in weights]

        if not check_n_channels(self.n_channels):
            # The number of channels is expected to be incorrect when the header is actually in v2 format
            return False

        if check_frequency(self.first_channel_frequency) and check_channel_bandwidth(
            self.channel_bandwidth
        ):
            return True
        else:
            if check_subband(self.subband):
                self.logger.info(f"Found subband {self.subband} in header")
            else:
                self.subband = self.extract_subband(self.filename)
                if check_subband(self.subband):
                    self.logger.info(f"Found subband {self.subband} in filename")
            sample_frequency = get_sample_frequency()
            self.channel_bandwidth = compute_channel_bandwidth(sample_frequency)
            self.first_channel_frequency = (
                self.channel_bandwidth * self.subband
            ) + get_frequency_offset()

            if check_frequency(
                self.first_channel_frequency
            ) and check_channel_bandwidth(self.channel_bandwidth):
                self.logger.info(
                    f"Computed frequency: {self.first_channel_frequency} MHz, bandwidth: {self.channel_bandwidth} Mhz"
                )
                return True

        return False

    def read_v2(self, file):
        header_format = self.get_header_format_v2()
        header_size = struct.calcsize(header_format)

        header_data = file.read(header_size)

        if len(header_data) < header_size:
            return False

        header = struct.unpack(header_format, header_data)
        (
            self.magic,
            self.n_receivers,
            self.n_correlations,
            self.correlation_mode,
            self.start_time,
            self.end_time,
            *weights,
            self.n_samples_per_integration,
            self.n_channels,
            self.first_channel_frequency,
            self.channel_bandwidth,
        ) = header
        self.weights = [int(weight) for weight in weights]

        if not check_n_channels(self.n_channels):
            self.logger.error(f"Incorrect number of channels read: {self.n_channels}")
            return False


        return True

    def to_string(self, indentation="  "):
        result = list()
        result.append(f"Magic: {self.magic}")
        result.append(f"Number of receivers: {self.n_receivers}")
        result.append(f"Number of polarizations: {self.n_correlations}")
        result.append(f"Correlation mode: {self.correlation_mode}")
        result.append(f"Start time: {self.start_time}")
        result.append(f"End time: {self.end_time}")
        result.append(f"Duration: {self.end_time - self.start_time} s")
        result.append(f"Weights: {list_to_string(self.weights)}")
        result.append(
            f"Number of samples per integration: {self.n_samples_per_integration}"
        )
        result.append(f"Number of channels: {self.n_channels}")
        result.append(
            f"First channel frequency: {self.first_channel_frequency*1e-6} MHz"
        )
        result.append(f"Channel bandwidth: {self.channel_bandwidth*1e-3} KHz")
        result = [f"{indentation}{x}" for x in result]
        return "\n".join(result)

    def write(self, file):
        header_format = self.get_header_format_v2()

        header_data = (
            self.magic,
            self.n_receivers,
            self.n_correlations,
            self.correlation_mode,
            self.start_time,
            self.end_time,
            *self.weights,
            self.n_samples_per_integration,
            self.n_channels,
            self.first_channel_frequency,
            self.channel_bandwidth,
        )

        header = struct.pack(header_format, *header_data)
        file.write(header)

    def diff(self, other):
        result = deepcopy(self)
        result.magic -= other.magic
        result.n_receivers -= other.n_receivers
        result.n_correlations -= other.n_correlations
        result.correlation_mode -= other.correlation_mode
        result.start_time -= other.start_time
        result.end_time -= other.end_time
        result.weights = [x[0] - x[1] for x in zip(result.weights, other.weights)]
        result.n_samples_per_integration -= other.n_samples_per_integration
        result.n_channels -= other.n_channels
        result.first_channel_frequency -= other.first_channel_frequency
        result.channel_bandwidth -= other.channel_bandwidth
        return result
