#
# AARTFAAC specific helper functions
#
# See the following page for more information about LOFAR subbands and frequencies:
# https://science.astron.nl/telescopes/lofar/lofar-system-overview/technical-specification/frequency-subband-selection-and-rfi/
#
def compute_channel_bandwidth(sample_frequency):
    if not (sample_frequency == 200) or (sample_frequency == 160):
        raise ValueError(
            f"Channel bandwidth unknown for sampling frequency of {sample_frequency} Mhz."
        )
    else:
        return (sample_frequency / 1024.0) * 1e6


def get_sample_frequency():
    # The sampling frequencies for LOFAR are:
    #  - 200 Mhz for LBA, HBA110_190 and HBA210_270
    #  - 160 Mhz for HBA170_230
    # AARTFAAC is LBA only, so always return 200 MHz
    return 200  # MHz


def get_frequency_offset():
    # The frequency offsets for LOFAR are:
    # - 0 MHz for LBA
    # - 100 MHz for HBA110_190
    # - 160 MHz for HBA170_230
    # - 200 MHz for HBA210_270
    # AARTFAAC is LBA only, so always return 0 MHz
    return 0  # MHz


def get_frequency_lower_limit():
    return 30 * 1e6  # MHz


def get_frequency_upper_limit():
    return 78 * 1e6  # MHz


def check_frequency(first_channel_frequency):
    return (first_channel_frequency >= get_frequency_lower_limit()) and (
        first_channel_frequency < get_frequency_upper_limit()
    )


def check_channel_bandwidth(channel_bandwidth):
    return channel_bandwidth == compute_channel_bandwidth(get_sample_frequency())


def check_subband(subband):
    subband_lower_limit = 154  # LBA 30 MHz
    subband_upper_limit = 397  # LBA 78 MHz
    return (subband >= subband_lower_limit) and (subband < subband_upper_limit)


def check_n_channels(n_channels):
    return (n_channels >= 1) and (n_channels <= 1024)
