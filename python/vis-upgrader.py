#!/usr/bin/env python3

import argparse
import logging

from aartfaac_header import *
from aartfaac_helper import *

if __name__ == "__main__":
    logger = logging.getLogger()
    logging.basicConfig(format="[%(levelname)s] %(message)s", level=logging.DEBUG)

    parser = argparse.ArgumentParser(description="Read and process data with header.")
    parser.add_argument(
        "-i", "--input", help="Path to the input data file", required=True
    )
    parser.add_argument(
        "-o", "--output", help="Path to the output data file", required=False
    )
    args = parser.parse_args()
    input_file_path = args.input
    output_file_path = args.output

    with open(input_file_path, "rb") as file_input:
        file_output = None
        print(input_file_path, output_file_path)
        if output_file_path:
            print(f"Reading from {input_file_path}, writing to {output_file_path}")
            file_output = open(output_file_path, "wb")

        nth_block = 0
        while True:
            try:
                header = Header(logger, input_file_path, file_input)
            except:
                break

            # Read the data assuming 4-byte complex float
            n_baselines = int(header.n_receivers * (header.n_receivers + 1) / 2)
            nVisibilities = n_baselines * header.n_channels * header.n_correlations

            data_size = nVisibilities * 2 * 4
            data_block = file_input.read(data_size)
            logger.info(f"This block has {nVisibilities} visibilities.")
            if data_size != len(data_block):
                logger.warning(
                    f"Mismatch in data size, expected {data_size}, but got {len(data_block)} bytes."
                )
                break

            # Print header
            print()
            print(f">>> Block {nth_block}:")
            print(header.to_string())
            print()

            if file_output:
                header.write(file_output)
                file_output.write(data_block)

            nth_block += 1
