#include <stdexcept>
#include <thread>
#include <sys/wait.h>
#include <values.h>
#include "stream_aggregator.h"
#include "common/aartfaac/filedescriptors.h"
#include "lib/aartfaac2ms/aartfaac2ms.h"

#define NANOSECONDS_PER_SECOND 1e9

void dispatchAf2ms(AartfaacHeader header,
                   std::vector<std::complex<float>> buffer) {
  Aartfaac2ms af2ms;
  AartfaacBuffer input(header, &buffer, AartfaacMode::FromNumber(1));

  std::cout << "[" << getpid() << "] Processing aartfaac2ms on databuffer."
            << std::endl;
}

void StreamAggregator::setMode(AartfaacMode mode) { _mode = mode; }
void StreamAggregator::setStreams(const string& streamDescriptors) {
  if (_mode == AartfaacMode::Unused) {
    throw std::runtime_error(
        "Set the aartfaacmode before parsing other arguments.");
  }

  std::vector<string> pipes = split(streamDescriptors, ",");
  for (string pipe : pipes) {
    _streams.push_back(std::ifstream(pipe));
  }
}

enum StreamState { NONE = 0, HEADER = 1, ALL = 2, FUTURE = 3 };
string stringStreamStates(std::vector<StreamState> states) {
  std::stringstream output;
  for (size_t i = 0; i < states.size(); i++) {
    auto state = states[i];

    output << std::to_string(i) << ":";
    if (state == NONE) output << "N";
    if (state == HEADER) output << "H";
    if (state == ALL) output << "A";
    if (state == FUTURE) output << "F";
    output << " ";
  }
  return output.str();
}

StreamAggregator::StreamAggregator() {}
void StreamAggregator::run() {
  // Storage for all input streams.
  std::vector<std::complex<BufType>> streamBuffer;
  std::vector<AartfaacHeader> headerBuffers(_streams.size());
  std::vector<typeof(streamBuffer)> dataBuffers(_streams.size());

  // Pointers to how many bytes in total were extracted from every stream.
  std::vector<size_t> bytesRead(_streams.size());
  std::vector<StreamState> streamStates(_streams.size(), StreamState::NONE);

  // TODO: Have a way of terminating this process.
  bool running = true;

  auto startTime =
      std::chrono::high_resolution_clock::now().time_since_epoch().count();
  double maxWaitTime = 1.1 * NANOSECONDS_PER_SECOND;  // nanoseconds
  double lastProcessedStartTime = 0;
  size_t nProcessed = 0;

  // We will wait forever until a header tells us what the actual interval is.
  double timeStepInterval = MAXDOUBLE;
  size_t nConsecutiveNoData = 0;

  std::cout << "Started processing streams at " << startTime << std::endl;
  while (running) {
    for (size_t sid = 0; sid < _streams.size(); sid++) {
      if (streamStates[sid] == ALL) continue;

      std::ifstream& stream = _streams[sid];

      /*
       * Read the header and set metadata.
       */
      if (streamStates[sid] == NONE) {
        char* buffer = reinterpret_cast<char*>(&headerBuffers[sid]);

        bytesRead[sid] += stream.readsome(
            buffer + bytesRead[sid], sizeof(AartfaacHeader) - bytesRead[sid]);

        if (bytesRead[sid] == sizeof(AartfaacHeader)) {
          streamStates[sid] = HEADER;

          if (timeStepInterval == MAXDOUBLE) {
            // The timestep interval can at the earliest be derived from the
            // first header packet we received.
            timeStepInterval =
                headerBuffers[sid].EndTime() - headerBuffers[sid].StartTime();
          }
        }
      }

      /*
       * Read the rest of the data chunks immediately
       */
      if (streamStates[sid] == HEADER) {
        // Ensure data will fit in this buffer.
        size_t dataByteChunk =
            headerBuffers[sid].VisPerTimestep() * sizeof(std::complex<BufType>);

        dataBuffers[sid].resize(dataByteChunk);

        // Read data into buffer, and increment bytes read.
        char* buffer = reinterpret_cast<char*>(dataBuffers[sid].data());
        bytesRead[sid] += stream.readsome(
            buffer + bytesRead[sid] - sizeof(AartfaacHeader),
            dataByteChunk - (bytesRead[sid] - sizeof(AartfaacHeader)));

        if (bytesRead[sid] == (dataByteChunk + sizeof(AartfaacHeader))) {
          streamStates[sid] = ALL;

          // If a full chunk is read, and the timestep is old, throw out this
          // data.
          if (headerBuffers[sid].StartTime() < lastProcessedStartTime) {
            streamStates[sid] = NONE;
          }
        }
      }
    }

    // If all streams received data, we can start processing.
    bool done =
        std::all_of(streamStates.begin(), streamStates.end(),
                    [](StreamState v) { return v == ALL || v == FUTURE; });

    auto currentTime =
        std::chrono::high_resolution_clock::now().time_since_epoch().count();

    size_t totalTimeSpent = currentTime - startTime;

    if (done || totalTimeSpent > (maxWaitTime + nProcessed * timeStepInterval *
                                                    NANOSECONDS_PER_SECOND)) {
      if (not done) {
        std::cout << "WARNING: Some streams were not fully completed."
                  << std::endl;
      }
      nProcessed++;

      /*
       * Find oldest stream out of to-be-processed streams.
       */
      int minSid = -1;
      double minValue = MAXDOUBLE;
      for (size_t sid = 0; sid < headerBuffers.size(); sid++) {
        // Select the lowest time out of all streams that received data.
        if (streamStates[sid] == StreamState::ALL &&
            minValue > headerBuffers[sid].StartTime()) {
          minValue = headerBuffers[sid].StartTime();
          minSid = sid;
        }
      }
      if (minSid == -1) {
        std::cout << "Warning: no streams received for t="
                  << std::to_string(startTime + nProcessed * timeStepInterval *
                                                    NANOSECONDS_PER_SECOND)
                  << std::endl;
        nConsecutiveNoData++;

        if (nConsecutiveNoData == _nConsecutiveNoData) {
          return;
        }

        sleep(_timeSkip);
        continue;
      }
      nConsecutiveNoData = 0;

      for (size_t sid = 0; sid < headerBuffers.size(); sid++) {
        if (headerBuffers[sid].StartTime() >
            headerBuffers[minSid].StartTime()) {
          streamStates[sid] = StreamState::FUTURE;
        }
      }
      /*
       * Process the data in af2ms.
       */
      std::cout << "Processing timestep: "
                << std::to_string(headerBuffers[minSid].StartTime()) << "\n"
                << stringStreamStates(streamStates) << std::endl;

      // TODO: Run af2ms here on all streams with timestep == min->StartTime()
      std::cout << "Dispatching af2ms..." << std::endl;

      for (size_t sid = 0; sid < _streams.size(); sid++) {
        size_t id = fork();
        if (id == 0) {
          dispatchAf2ms(headerBuffers[sid], dataBuffers[sid]);
          exit(EXIT_SUCCESS);
        }
      }

      if (nProcessed == 6) {
        std::cout << "Done processing..." << std::endl;
        running = false;
      }

      /*
       * Reset buffer trackers for all streams which have been processed.
       */
      for (size_t sid = 0; sid < _streams.size(); sid++) {
        if (headerBuffers[sid].StartTime() <=
            headerBuffers[minSid].StartTime()) {
          streamStates[sid] = StreamState::NONE;
          bytesRead[sid] = StreamState::NONE;
        }
      }
    }
  }

  // TODO: wait for all children to end to ensure no orphan processes remain.
}
