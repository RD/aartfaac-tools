#ifndef AARTFAAC2MS_STREAM_AGGREGATOR_H
#define AARTFAAC2MS_STREAM_AGGREGATOR_H

#include <vector>
#include "common/aartfaac/aartfaacinput.h"

class StreamAggregator {
  using BufType = float;

 public:
  StreamAggregator();

  void run();

  void setTimeskip(double d) { _timeSkip = d; };
  void setNConsecutiveNoData(size_t n) { _nConsecutiveNoData = n; };
  void setStreams(const std::string &streamDescriptors);
  void setMode(AartfaacMode _mode);

 private:
  std::vector<std::ifstream> _streams;
  AartfaacMode _mode;
  double _timeSkip;
  size_t _nConsecutiveNoData;
};

#endif  // AARTFAAC2MS_STREAM_AGGREGATOR_H
