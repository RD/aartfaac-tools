#include "parameters_stream.h"

po::options_description StreamParameters::getDescription() {
  po::options_description description("Stream Aggregator");
  description.add_options()(
      "packetloss_threshold",
      po::value<size_t>(&_settings.nConsecutiveNoData)->default_value(8),
      "The amount of timesteps no packets need to arrive for the program to "
      "stop, default is 8.")(
      "timeskip", po::value<double>(&_settings.timeSkip)->default_value(1.),
      "How long to wait (in seconds) after not receiving data before "
      "re-polling the streams. Default is 1 second.");

  return description;
}
po::positional_options_description StreamParameters::getPositional() {
  return po::positional_options_description();
}
void StreamParameters::parseArgs(const po::variables_map& vm) {
  if (vm.count("mode")) {
    // TODO: Remove mode from streamparser
    // _settings.mode = AartfaacMode::FromNumber(vm.)
  }
}
void StreamParameters::configure(StreamAggregator& aggregator) {
  aggregator.setTimeskip(_settings.timeSkip);
  aggregator.setNConsecutiveNoData(_settings.nConsecutiveNoData);
}
