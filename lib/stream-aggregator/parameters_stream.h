#ifndef STREAM_PARAMETERS_H
#define STREAM_PARAMETERS_H

#include "common/parameter_provider.h"
#include "stream_aggregator.h"

class StreamParameters : public ParameterProviderArguments {
 public:
  po::options_description getDescription() override;
  po::positional_options_description getPositional() override;
  void parseArgs(const po::variables_map& vm) override;

  struct Settings {
    size_t nConsecutiveNoData;
    double timeSkip;
    std::string streamNames;
    int mode;
  } _settings;
  void configure(StreamAggregator& aggregator);
};

#endif  // STREAM_PARAMETERS_H
