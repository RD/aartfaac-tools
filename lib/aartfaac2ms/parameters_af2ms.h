#ifndef AF2MS_PARAMETERPROVIDER_H
#define AF2MS_PARAMETERPROVIDER_H

#include <boost/program_options.hpp>
#include <unistd.h>
#include <utility>

#include "common/aartfaac/aartfaacmode.h"
#include "aartfaac2ms.h"
#include "common/parameter_provider.h"

namespace po = boost::program_options;
class Aartfaac2msParameters : public ParameterProviderArguments {
 public:
  po::options_description getDescription() override;
  po::positional_options_description getPositional() override;
  void parseArgs(const po::variables_map& vm) override;

  // Configures the aartfaac2ms object with the parsed parameters.
  void configure(Aartfaac2ms& af2ms, po::variables_map vm) const;

  /*
   * Configuration parameter getters.
   */
  std::string& getInputFilename() { return _settings.inputFilename; }
  std::string& getOutputFilename() { return _settings.outputFilename; }
  std::string& getAntennaConfFilename() {
    return _settings.antennaConfFilename;
  }

 private:
  /*
   * Data containers
   */
  // These are the raw argument values, to be parsed by parseArgs()
  struct Arguments {
    int mode;
    std::vector<size_t> interval;
    std::vector<std::string> phaseCentreRaDec;
    std::vector<std::string> dyscoConfig;
  } _arguments;

  // After calling parseArgs, these settings are set
  struct Settings {
    std::string inputFilename;
    std::string outputFilename;
    std::string antennaConfFilename;
    double memPercentage;
    size_t threadCount;

    std::pair<size_t, size_t> interval;
    std::pair<double, double> phaseCentreRaDec;
    AartfaacMode mode;
    bool rfiDetection;
    bool collectStatistics;
    bool useDysco;

    struct Dysco {
      size_t dataBitrate;
      size_t weightBitrate;
      std::string distribution;
      double distTruncation;
      std::string normalization;
    } dysco;
  } _settings;
};

#endif  // AF2MS_PARAMETERPROVIDER_H
