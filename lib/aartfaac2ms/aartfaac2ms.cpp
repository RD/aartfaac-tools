#include "aartfaac2ms.h"

#include "aartfaacms.h"
#include "common/io/averagingwriter.h"
#include "common/io/fitswriter.h"
#include "common/io/mswriter.h"
#include "common/io/threadedwriter.h"
#include "version.h"

#include "units/radeccoord.h"
#include "common/fixed_tokens_value.h"
#include "common/baseline_utils.h"

#include <casacore/measures/Measures/MBaseline.h>
#include <casacore/measures/Measures/MCBaseline.h>
#include <casacore/measures/Measures/MCDirection.h>
#include <casacore/measures/Measures/MeasConvert.h>
#include <casacore/measures/Measures/MEpoch.h>
#include <casacore/measures/Measures/MPosition.h>
#include <casacore/measures/Measures/Muvw.h>

#include <complex>
#include <iostream>
#include <fstream>

#include <unistd.h>

#include <xmmintrin.h>
#if defined(__FMA__)
#include <immintrin.h>
#endif

// USE_SSE is disabled by default, because the non-vectorized code
// performs slightly better and is easier to read.
//#define USE_SSE

#define SPEED_OF_LIGHT 299792458.0  // speed of light in m/s

using namespace aoflagger;

Aartfaac2ms::Aartfaac2ms()
    : _flagger(),
      _mode(AartfaacMode::Unused),
      _outputFormat(MSOutputFormat),
      _rfiDetection(false),
      _collectStatistics(false),
      _collectHistograms(false),
      _timeAvgFactor(1),
      _freqAvgFactor(1),
      _memPercentage(50),
      _intervalStart(0),
      _intervalEnd(0),
      _manualPhaseCentre(false),
      _manualPhaseCentreRA(0.0),
      _manualPhaseCentreDec(0.0),
      _useDysco(false),
      _dyscoDataBitRate(8),
      _dyscoWeightBitRate(12),
      _dyscoDistribution("TruncatedGaussian"),
      _dyscoNormalization("AF"),
      _dyscoDistTruncation(2.5),
      _threadCount(1),
      _outputData(empty_aligned<std::complex<float>>()),
      _outputWeights(empty_aligned<float>()) {}

void Aartfaac2ms::allocateBuffers() {
  bool rfiDetection = false;
  double memLimit = 0.0;
  long int pageCount = sysconf(_SC_PHYS_PAGES),
           pageSize = sysconf(_SC_PAGE_SIZE);
  int64_t memSize = (int64_t)pageCount * (int64_t)pageSize;
  double memSizeInGB = (double)memSize / (1024.0 * 1024.0 * 1024.0);
  double memPercentage = _memPercentage;
  if (memLimit == 0.0)
    std::cout << "Detected " << round(memSizeInGB * 10.0) / 10.0
              << " GB of system memory.\n";
  else {
    std::cout << "Using " << round(memLimit * 10.0) / 10.0 << '/'
              << round(memSizeInGB * 10.0) / 10.0 << " GB of system memory.\n";
    memSize = int64_t(memLimit * (1024.0 * 1024.0 * 1024.0));
    memPercentage = 100.0;
  }
  size_t nChannelSpace = ((((_reader->NChannels() - 1) / 4) + 1) * 4);
  size_t maxSamples = memSize * memPercentage / (100 * (sizeof(float) * 2 + 1));
  size_t nAntennas = _reader->NAntennas();
  size_t maxScansPerPart =
      maxSamples / (4 * nChannelSpace * (nAntennas + 1) * nAntennas / 2);
  std::cout << "Timesteps that fit in memory: " << maxScansPerPart << '\n';
  if (maxScansPerPart < 1) {
    std::cout << "WARNING! The given amount of memory is not even enough for "
                 "one scan and therefore below the minimum that Aartfaac2ms "
                 "will need; will use more memory. Expect swapping and very "
                 "poor flagging accuracy.\nWARNING! This is a *VERY BAD* "
                 "condition, so better make sure to resolve it!";
    maxScansPerPart = 1;
  } else if (maxScansPerPart < 20 && rfiDetection) {
    std::cout << "WARNING! This computer does not have enough memory for "
                 "accurate flagging; expect non-optimal flagging accuracy.\n";
  }
  size_t nTimesteps = NTimestepsSelected();
  _nParts = 1 + nTimesteps / maxScansPerPart;
  if (_nParts == 1)
    std::cout << "All " << nTimesteps
              << " scans fit in memory; no partitioning necessary.\n";
  else
    std::cout
        << "Observation does not fit fully in memory, will partition data in "
        << _nParts << " chunks of " << (nTimesteps / _nParts) << " scans.\n";

  // Pre-allocate vis buffers.
  _visibilityDataBuffer.resize((nTimesteps / _nParts));
  for (std::vector<std::complex<float>> &vis : _visibilityDataBuffer) {
    vis.resize(_reader->VisPerTimestep());
  }
}

void Aartfaac2ms::initializeWriter(const char *outputFilename) {
  switch (_outputFormat) {
    case FitsOutputFormat:
      _writer.reset(new ThreadedWriter(
          std::unique_ptr<Writer>(new FitsWriter(outputFilename))));
      break;
    case MSOutputFormat: {
      std::unique_ptr<MSWriter> msWriter(new MSWriter(outputFilename));
      if (_useDysco)
        msWriter->EnableCompression(_dyscoDataBitRate, _dyscoWeightBitRate,
                                    _dyscoDistribution, _dyscoDistTruncation,
                                    _dyscoNormalization);
      _writer.reset(new ForwardingWriter(std::move(msWriter)));
    } break;
  }

  if (_freqAvgFactor != 1 || _timeAvgFactor != 1) {
    _writer.reset(
        new ThreadedWriter(std::unique_ptr<Writer>(new AveragingWriter(
            std::move(_writer), _timeAvgFactor, _freqAvgFactor))));
  }

  setAntennas();
  setSPWs();
  setSource();
  setField();
  _writer->WritePolarizationForLinearPols(false);
  setObservation();
}

void Aartfaac2ms::setAntennas() {
  std::vector<Writer::AntennaInfo> antennas(_reader->NAntennas());
  for (size_t ant = 0; ant != antennas.size(); ++ant) {
    Writer::AntennaInfo &antennaInfo = antennas[ant];
    antennaInfo.name = std::string("A12_") + std::to_string(ant);
    antennaInfo.station = "AARTFAAC";
    antennaInfo.type = "GROUND-BASED";
    antennaInfo.mount =
        "ALT-AZ";  // TODO should be "FIXED", but Casa does not like
    antennaInfo.x = _antennaPositions[ant].getValue()(0);
    antennaInfo.y = _antennaPositions[ant].getValue()(1);
    antennaInfo.z = _antennaPositions[ant].getValue()(2);
    antennaInfo.diameter = 1; /** TODO can probably give more exact size! */
    antennaInfo.flag = false;
  }
  _writer->WriteAntennae(antennas, _reader->StartTime());
}

void Aartfaac2ms::setSPWs() {
  std::vector<MSWriter::ChannelInfo> channels(_reader->NChannels());
  _channelFrequenciesHz.resize(_reader->NChannels());
  std::ostringstream str;
  str << "AARTF_BAND_" << (round(1e-6 * _reader->Frequency() * 10.0) / 10.0);
  const double chWidth = _reader->Bandwidth() / _reader->NChannels();
  double startFrequency = _reader->Frequency() - _reader->Bandwidth() * 0.5;
  for (size_t ch = 0; ch != channels.size(); ++ch) {
    MSWriter::ChannelInfo &channel = channels[ch];
    _channelFrequenciesHz[ch] = startFrequency + chWidth * (0.5 + double(ch));
    channel.chanFreq = _channelFrequenciesHz[ch];
    channel.chanWidth = chWidth;
    channel.effectiveBW = chWidth;
    channel.resolution = chWidth;
  }
  _writer->WriteBandInfo(str.str(), channels, _reader->Frequency(),
                         _reader->Bandwidth(), false);
}

void Aartfaac2ms::setSource() {
  MSWriter::SourceInfo source;
  source.sourceId = 0;
  source.time = _reader->StartTime();
  source.interval =
      _reader->StartTime() + _reader->IntegrationTime() * _reader->NTimesteps();
  source.spectralWindowId = 0;
  source.numLines = 0;
  source.name = "AARTFAAC";
  source.calibrationGroup = 0;
  source.code = "";
  double ra = _phaseDirection.getAngle().getValue()[0];
  double dec = _phaseDirection.getAngle().getValue()[1];
  source.directionRA = ra;  // (in radians)
  source.directionDec = dec;
  source.properMotion[0] = 0.0;
  source.properMotion[1] = 0.0;
  _writer->WriteSource(source);
}

void Aartfaac2ms::setField() {
  MSWriter::FieldInfo field;
  field.name = "AARTFAAC";
  field.code = std::string();
  field.time = _reader->StartTime();
  field.numPoly = 0;
  double ra = _phaseDirection.getAngle().getValue()[0];
  double dec = _phaseDirection.getAngle().getValue()[1];
  field.delayDirRA = ra;  // (in radians)
  field.delayDirDec = dec;
  field.phaseDirRA = field.delayDirRA;
  field.phaseDirDec = field.delayDirDec;
  field.referenceDirRA = field.delayDirRA;
  field.referenceDirDec = field.delayDirDec;
  field.sourceId = -1;
  field.flagRow = false;
  _writer->WriteField(field);
}

void Aartfaac2ms::setObservation() {
  Writer::ObservationInfo observation;
  observation.telescopeName = "AARTFAAC";
  observation.startTime = _reader->StartTime();
  observation.endTime =
      _reader->StartTime() + _reader->IntegrationTime() * _reader->NTimesteps();
  observation.observer = "Unknown";
  observation.scheduleType = "AARTFAAC";
  observation.project = "Unknown";
  observation.releaseDate = 0;
  observation.flagRow = false;

  _writer->WriteObservation(observation);
}

void Aartfaac2ms::processBaseline(size_t baselineIndex,
                                  Strategy &threadStrategy,
                                  QualityStatistics &threadStatistics) {
  FlagMask &flagMask = _flagBuffers[baselineIndex];
  const std::pair<size_t, size_t> &baseline =
      compute_baseline(baselineIndex, _reader->NAntennas());

  if (_rfiDetection && (baseline.first != baseline.second)) {
    flagMask = threadStrategy.Run(_imageSetBuffers[baselineIndex]);
  } else {
    flagMask = _flagger.MakeFlagMask(_timestepsStart.size(),
                                     _channelFrequenciesHz.size(), false);
  }
  if (_collectStatistics) {
    threadStatistics.CollectStatistics(_imageSetBuffers[baselineIndex],
                                       flagMask, _correlatorMask,
                                       baseline.first, baseline.second);
  }
}

void Aartfaac2ms::Run(const char *inputFilename, const char *outputFilename,
                      const char *antennaConfFilename) {
  if (_mode == AartfaacMode::Unused) {
    throw std::logic_error("No valid aartfaac mode set before calling Run.");
  }

  auto initializationWatch = Stopwatch();
  initializationWatch.Start();

  _reader.reset(new AartfaacFile(inputFilename, _mode));

  readAntennaPositions(antennaConfFilename);

  if (_rfiDetection) {
    _strategyFile =
        _flagger.FindStrategyFile(aoflagger::TelescopeId::AARTFAAC_TELESCOPE);
  }

  allocateBuffers();

  initializeWriter(outputFilename);

  _reader->SeekToTimestep(_intervalStart);

  for (size_t chunkIndex = 0; chunkIndex != _nParts; ++chunkIndex) {
    std::cout << "=== Processing chunk " << (chunkIndex + 1) << " of "
              << _nParts << " ===\n";

    size_t nTimesteps = NTimestepsSelected();
    size_t chunkStart = nTimesteps * chunkIndex / _nParts + _intervalStart;
    size_t chunkEnd = nTimesteps * (chunkIndex + 1) / _nParts + _intervalStart;

    if (_collectStatistics) {
      // The correlatormask is exclusively used for collection of quality
      // statistics.
      _correlatorMask = _flagger.MakeFlagMask(chunkEnd - chunkStart,
                                              _reader->NChannels(), false);
    }

    /*
     * ImageSetBuffers initialization for backwards compatibility with original
     * flagging-data collection
     */
    size_t nAntennas = _reader->NAntennas();
    size_t nBaselines = compute_nr_baselines(nAntennas);

    // We only need to allocate _imageSetBuffers if we do statistics collection
    // or rfidetection.
    if (_rfiDetection or _collectStatistics) {
      const size_t requiredWidthCapacity = (nTimesteps + _nParts - 1) / _nParts;
      _imageSetBuffers.resize(nBaselines);

      // Create imageset for each baseline.
      for (size_t bl = 0; bl < nBaselines; bl++) {
        _imageSetBuffers[bl] =
            _flagger.MakeImageSet(requiredWidthCapacity, _reader->NChannels(),
                                  8, 0.0f, requiredWidthCapacity);
      }
      // Ensure every baseline imageset can hold all timesteps of data.
      for (ImageSet &imageSet : _imageSetBuffers) {
        imageSet.ResizeWithoutReallocation(chunkEnd - chunkStart);
      }
    }

    initializationWatch.Pause();

    _readWatch.Start();
    _timestepsStart.clear();
    _timestepsEnd.clear();

    ProgressBar progress("Reading");
    for (size_t timeIndex = chunkStart; timeIndex != chunkEnd; ++timeIndex) {
      progress.SetProgress(timeIndex - chunkStart, chunkEnd - chunkStart);

      // Initialize buffer for reshuffling.
      std::vector<std::complex<float>> visBufferPtr(_reader->VisPerTimestep());
      Timestep step = _reader->ReadTimestep(visBufferPtr.data());
      _timestepsStart.emplace_back(step.startTime);
      _timestepsEnd.emplace_back(step.endTime);

      std::complex<float> *diskVisPtr = visBufferPtr.data();
      size_t bufferIndex = timeIndex - chunkStart;
      size_t nChannels = _reader->NChannels();
      size_t nPolarizations = _reader->NPolarizations();
      for (size_t antenna1 = 0; antenna1 != nAntennas; ++antenna1) {
        for (size_t antenna2 = 0; antenna2 <= antenna1; ++antenna2) {
          size_t bIndex = compute_baseline_index(antenna2, antenna1, nAntennas);

          std::complex<float> *visDataPtr =
              &_visibilityDataBuffer[bufferIndex]
                                    [bIndex * (nChannels * nPolarizations)];

          // Write data into the _visibilityDataBuffer in a different baseline
          // ordering + channel/polarization ordering.
          for (size_t ch = 0; ch != nChannels; ++ch) {
            for (size_t p = 0; p != nPolarizations; ++p) {
              visDataPtr[p * nChannels + ch] =
                  diskVisPtr[ch * nPolarizations + p];
            }
          }

          if (_rfiDetection or _collectStatistics) {
            /*
             * ImageSetBuffers are only used in the case of RFI detection and
             * statistics collection. Otherwise, this can be skipped, and as
             * such save time.
             */
            ImageSet &imageSet = _imageSetBuffers[bIndex];
            for (size_t ch = 0; ch != _reader->NChannels(); ++ch) {
              for (size_t p = 0; p != 4; ++p) {
                float *realPtr = imageSet.ImageBuffer(p * 2) + bufferIndex,
                      *imagPtr = imageSet.ImageBuffer(p * 2 + 1) + bufferIndex;
                realPtr[ch * imageSet.HorizontalStride()] = diskVisPtr->real();
                imagPtr[ch * imageSet.HorizontalStride()] = diskVisPtr->imag();
                ++diskVisPtr;
              }
            }
          } else {
            // If not entering the imageSetBuffer loop, we can just increment
            // the pointer.
            diskVisPtr += nChannels * nPolarizations;
          }
        }
      }
    }

    _readWatch.Pause();

    _processWatch.Start();
    progress = ProgressBar("Processing baselines");

    _flagBuffers.clear();
    _flagBuffers.resize(nBaselines);

    aoflagger::Strategy strategy;
    if (_rfiDetection) {
      strategy = _flagger.LoadStrategyFile(_strategyFile);
    }

    QualityStatistics threadStatistics;
    // We don't always want to collect QualityStatistics, but the object does
    // need to be passed.
    if (_collectStatistics) {
      threadStatistics = _flagger.MakeQualityStatistics(
          _timestepsStart.data(), _timestepsStart.size(),
          &_channelFrequenciesHz[0], _channelFrequenciesHz.size(), 4,
          _collectHistograms);
    }

    for (size_t baselineIndex = 0;
         baselineIndex != compute_nr_baselines(_reader->NAntennas());
         ++baselineIndex) {
      processBaseline(baselineIndex, strategy, threadStatistics);
    }

    // We don't always want to collect QualityStatistics.
    if (_collectStatistics) {
      // Mutex needs to be locked
      std::unique_lock<std::mutex> lock(_mutex);
      if (_statistics == nullptr)
        _statistics.reset(new QualityStatistics(threadStatistics));
      else
        (*_statistics) += threadStatistics;
    }

    _processWatch.Pause();
    _writeWatch.Start();
    _outputFlags.resize(_reader->NChannels() * 4);
    _outputData =
        make_aligned<std::complex<float>>(_reader->NChannels() * 4, 16);
    _outputWeights = make_aligned<float>(_reader->NChannels() * 4, 16);

    for (size_t timeIndex = chunkStart; timeIndex != chunkEnd; ++timeIndex) {
      processAndWriteTimestep(timeIndex, chunkStart,
                              _visibilityDataBuffer[timeIndex]);
    }

    _writeWatch.Pause();
    std::cout << _writeWatch.ToString() << " ";
    _writeWatch.Start();

    _flagBuffers.clear();
  }

  // Frees the object (0.3s)
  _writer.reset();

  _writeWatch.Pause();
  std::cout << _writeWatch.ToString() << " ";
  _writeWatch.Start();
  if (_outputFormat == MSOutputFormat) {
    std::cout << "Writing AARTFAAC fields to measurement set...\n";
    writeAartfaacFieldsToMS(outputFilename, NTimestepsSelected() / _nParts);
  }
  _writeWatch.Pause();

  std::cout << "Init: " << initializationWatch.ToString()
            << ", read: " << _readWatch.ToString()
            << ", processing: " << _processWatch.ToString()
            << ", writing: " << _writeWatch.ToString() << '\n';

  if (_collectStatistics) {
    std::cout << "Writing statistics to measurement set...\n";
    _statistics->WriteStatistics(outputFilename);
  }
}

casacore::MVuvw calculateUVW(const casacore::MPosition &antennaPos,
                             const casacore::MPosition &refPos,
                             const casacore::MEpoch &time,
                             const casacore::MDirection &direction) {
  const casacore::Vector<double> posVec = antennaPos.getValue().getVector();
  const casacore::Vector<double> refVec = refPos.getValue().getVector();
  casacore::MVPosition relativePos(posVec[0] - refVec[0], posVec[1] - refVec[1],
                                   posVec[2] - refVec[2]);
  casacore::MeasFrame frame(time, refPos, direction);
  casacore::MBaseline baseline(
      casacore::MVBaseline(relativePos),
      casacore::MBaseline::Ref(casacore::MBaseline::ITRF, frame));
  casacore::MBaseline j2000Baseline =
      casacore::MBaseline::Convert(baseline, casacore::MBaseline::J2000)();
  casacore::MVuvw uvw(j2000Baseline.getValue(), direction.getValue());

  return uvw;
}

void Aartfaac2ms::processAndWriteTimestep(
    size_t timeIndex, size_t chunkStart,
    std::vector<std::complex<float>> &vis) {
  const size_t nAntennas = _reader->NAntennas();
  const size_t nChannels = _reader->NChannels();
  const size_t nPolarizations = _reader->NPolarizations();
  const size_t nBaselines = nAntennas * (nAntennas + 1) / 2;
  const double startTime = _timestepsStart[timeIndex - chunkStart];
  const double exposure = _timestepsEnd[timeIndex - chunkStart] - startTime;

  _uvws.resize(_reader->NAntennas());
  casacore::MEpoch timeEpoch = casacore::MEpoch(
      casacore::MVEpoch(startTime / 86400.0), casacore::MEpoch::UTC);

  auto reference = _antennaPositions[0];
  auto phaseDirection = _phaseDirection;

  // TODO: Parallelize this loop, as it is the highest time-share compute loop
  for (size_t antenna = 0; antenna != nAntennas; ++antenna) {
    casacore::MVuvw uvw = calculateUVW(_antennaPositions[antenna], reference,
                                       timeEpoch, phaseDirection)
                              .getValue();
    _uvws[antenna] =
        UVW{uvw.getVector()[0], uvw.getVector()[1], uvw.getVector()[2]};
  }

  _writer->AddRows(nBaselines);

  std::vector<float> cosAngles(nChannels), sinAngles(nChannels);

  initializeWeights(_outputWeights.get(), exposure);
  size_t baselineIndex = 0;
  for (size_t antenna1 = 0; antenna1 != nAntennas; ++antenna1) {
    for (size_t antenna2 = antenna1; antenna2 != nAntennas; ++antenna2) {
      const FlagMask &flagMask = _flagBuffers[baselineIndex];

      const size_t flagStride = flagMask.HorizontalStride();
      double u = _uvws[antenna1].u - _uvws[antenna2].u,
             v = _uvws[antenna1].v - _uvws[antenna2].v,
             w = _uvws[antenna1].w - _uvws[antenna2].w;

      // Pre-calculate rotation coefficients for geometric phase delay
      // correction
      for (size_t ch = 0; ch != nChannels; ++ch) {
        float angle =
            -2.0f * M_PI * w * _channelFrequenciesHz[ch] / SPEED_OF_LIGHT;
        sinAngles[ch] = sinf(angle);
        cosAngles[ch] = cosf(angle);
      }

      size_t bufferIndex = timeIndex - chunkStart;
#ifndef USE_SSE

      for (size_t p = 0; p != nPolarizations; ++p) {
        for (size_t ch = 0; ch != nChannels; ++ch) {
          size_t visIndex =
              ((baselineIndex * nPolarizations + p) * nChannels + ch);

          const float rtmp = vis[visIndex].real();
          const float itmp = vis[visIndex].imag();

          // Apply geometric phase delay (for w)
          _outputData[p + ch * nPolarizations] =
              std::complex<float>(cosAngles[ch] * rtmp - sinAngles[ch] * itmp,
                                  sinAngles[ch] * rtmp + cosAngles[ch] * itmp);

          std::copy_n(flagMask.Buffer() + bufferIndex + ch * flagStride,
                      nPolarizations,
                      _outputFlags.begin() + ch * nPolarizations);
        }
      }
#else
      for (size_t ch = 0; ch != nChannels; ++ch) {
        size_t offset = bufferIndex + ch * stride;
        const float *realAPtr = imageSet.ImageBuffer(0) + offset,
                    *imagAPtr = imageSet.ImageBuffer(1) + offset,
                    *realBPtr = imageSet.ImageBuffer(2) + offset,
                    *imagBPtr = imageSet.ImageBuffer(3) + offset,
                    *realCPtr = imageSet.ImageBuffer(4) + offset,
                    *imagCPtr = imageSet.ImageBuffer(5) + offset,
                    *realDPtr = imageSet.ImageBuffer(6) + offset,
                    *imagDPtr = imageSet.ImageBuffer(7) + offset;

        // Apply geometric phase delay (for w)
        // Note that order within set_ps is reversed; for the four complex
        // numbers, the first two compl are loaded corresponding to
        // set_ps(imag2, real2, imag1, real1).
        __m128 ra = _mm_set_ps(*realBPtr, *realBPtr, *realAPtr, *realAPtr);
        __m128 rb = _mm_set_ps(*realDPtr, *realDPtr, *realCPtr, *realCPtr);
        __m128 rgeom = _mm_set_ps(sinAngles[ch], cosAngles[ch], sinAngles[ch],
                                  cosAngles[ch]);
        __m128 ia = _mm_set_ps(*imagBPtr, *imagBPtr, *imagAPtr, *imagAPtr);
        __m128 ib = _mm_set_ps(*imagDPtr, *imagDPtr, *imagCPtr, *imagCPtr);
        __m128 igeom = _mm_set_ps(cosAngles[ch], -sinAngles[ch], cosAngles[ch],
                                  -sinAngles[ch]);
#if defined(__FMA__)
        __m128 outa = _mm_fmadd_ps(ra, rgeom, _mm_mul_ps(ia, igeom));
        __m128 outb = _mm_fmadd_ps(rb, rgeom, _mm_mul_ps(ib, igeom));
#else
        __m128 outa = _mm_add_ps(_mm_mul_ps(ra, rgeom), _mm_mul_ps(ia, igeom));
        __m128 outb = _mm_add_ps(_mm_mul_ps(rb, rgeom), _mm_mul_ps(ib, igeom));
#endif
        _mm_store_ps(reinterpret_cast<float *>(&_outputData[0] + ch * 4 + 0),
                     outa);
        _mm_store_ps(reinterpret_cast<float *>(&_outputData[0] + ch * 4 + 2),
                     outb);

        std::copy_n(flagMask.Buffer() + bufferIndex + ch * flagStride, 4,
                    _outputFlags.begin() + ch * 4);
      }
#endif

      _writer->WriteRow(startTime, startTime, antenna1, antenna2, u, v, w,
                        exposure, _outputData.get(), _outputFlags,
                        _outputWeights.get());
      ++baselineIndex;
    }
  }
}

void Aartfaac2ms::readAntennaPositions(const char *antennaConfFilename) {
  AntennaConfig antConf(antennaConfFilename);
  std::vector<Position> positions;
  switch (_mode.mode) {
    case AartfaacMode::LBAInner10_90:
    case AartfaacMode::LBAInner30_90:
    case AartfaacMode::LBAOuter10_90:
    case AartfaacMode::LBAOuter30_90:
      std::cout << "Using LBA antenna positions.\n";
      positions = antConf.GetLBAPositions();
      _antennaAxes = antConf.GetLBAAxes();
      break;
    case AartfaacMode::HBA110_190:
    case AartfaacMode::HBA170_230:
    case AartfaacMode::HBA210_270:
      std::cout << "Using HBA antenna positions.\n";
      positions = antConf.GetHBAPositions();
      _antennaAxes = antConf.GetHBA0Axes();
      break;
    default:
      throw std::runtime_error("Wrong RCU mode");
  }

  for (const Position &p : positions) {
    _antennaPositions.emplace_back(casacore::MVPosition(p.x, p.y, p.z),
                                   casacore::MPosition::ITRF);
  }

  size_t lastTimestep = _intervalEnd;
  if (lastTimestep == 0) lastTimestep = _reader->NTimesteps();
  _reader->SeekToTimestep((_intervalStart + lastTimestep) / 2);
  double centralTime = _reader->ReadMetadata().startTime;
  casacore::MEpoch time = casacore::MEpoch(
      casacore::MVEpoch(centralTime / 86400.0), casacore::MEpoch::UTC);
  casacore::MeasFrame frame(_antennaPositions[0], time);

  const casacore::MDirection::Ref azelRef(casacore::MDirection::AZEL, frame);
  const casacore::MDirection::Ref j2000Ref(casacore::MDirection::J2000, frame);
  casacore::MDirection zenithAzEl(casacore::MVDirection(0.0, 0.0, 1.0),
                                  azelRef);
  _phaseDirection = casacore::MDirection::Convert(zenithAzEl, j2000Ref)();
  double ra = _phaseDirection.getAngle().getValue()[0];
  double dec = _phaseDirection.getAngle().getValue()[1];
  std::cout << "Central time: " << time
            << ", zenith direction: " << RaDecCoord::RaDecToString(ra, dec)
            << '\n';
  if (_manualPhaseCentre) {
    _phaseDirection.set(
        casacore::MVDirection(_manualPhaseCentreRA, _manualPhaseCentreDec),
        j2000Ref);
    std::cout << "Using manual phase centre: "
              << RaDecCoord::RaDecToString(_manualPhaseCentreRA,
                                           _manualPhaseCentreDec)
              << '\n';
  } else {
    std::cout
        << "Zenith direction at central time is used as phase direction.\n";
  }
}

void Aartfaac2ms::initializeWeights(float *outputWeights,
                                    double integrationTime) {
  // Weights are normalized so that a 'reasonable' res of 200 kHz, 1s has
  // weight of "1" per sample.
  // Note that this only holds for numbers in the WEIGHTS_SPECTRUM column;
  // WEIGHTS will hold the sum.
  double weightFactor =
      integrationTime * (_reader->Bandwidth() / _reader->NChannels());
  for (size_t ch = 0; ch != _reader->NChannels(); ++ch) {
    for (size_t p = 0; p != 4; ++p) outputWeights[ch * 4 + p] = weightFactor;
  }
}

void Aartfaac2ms::writeAartfaacFieldsToMS(const std::string &outputFilename,
                                          size_t flagWindowSize) {
  AartfaacMS afMs(outputFilename);
  afMs.InitializeFields();
  const char *modeStr;
  switch (_mode.mode) {
    case AartfaacMode::LBAInner10_90:
    case AartfaacMode::LBAInner30_90:
    case AartfaacMode::LBAOuter10_90:
    case AartfaacMode::LBAOuter30_90:
      modeStr = "LBA";
      break;
    case AartfaacMode::HBA110_190:
    case AartfaacMode::HBA170_230:
    case AartfaacMode::HBA210_270:
      modeStr = "HBA";
      break;
    default:
      modeStr = "?";
      break;
  }
  afMs.UpdateObservationInfo(modeStr, _mode.mode, flagWindowSize);
  afMs.WriteKeywords(AF2MS_VERSION_STR, AF2MS_VERSION_DATE, _antennaAxes);
}
