#ifndef AARTFAAC2MS_H
#define AARTFAAC2MS_H

#include <aoflagger.h>

#include "antennaconfig.h"
#include "common/aligned_ptr.h"
#include "common/progressbar.h"
#include "common/stopwatch.h"
#include "common/aartfaac/aartfaacfile.h"
#include "common/io/averagingwriter.h"
#include "common/io/writer.h"

#include <casacore/measures/Measures/MDirection.h>
#include <casacore/measures/Measures/MPosition.h>

#include <complex>
#include <map>
#include <memory>
#include <mutex>
#include <vector>

struct UVW {
  double u, v, w;
};

class Aartfaac2ms {
 public:
  enum OutputFormat { MSOutputFormat, FitsOutputFormat };

  Aartfaac2ms();
  // TODO: Investigate why the aartfaac2ms destructor takes ~.3 seconds.

  void Run(const char* inputFilename, const char* outputFilename,
           const char* antennaConfFilename);

  void SetMemPercentage(double memPercentage) {
    _memPercentage = memPercentage;
  }
  void SetThreadCount(size_t nThreads) { _threadCount = nThreads; }
  void SetTimeAveraging(size_t factor) { _timeAvgFactor = factor; }
  void SetFrequencyAveraging(size_t factor) { _freqAvgFactor = factor; }
  void SetInterval(size_t start, size_t end) {
    _intervalStart = start;
    _intervalEnd = end;
  }
  void SetPhaseCentre(double ra, double dec) {
    _manualPhaseCentre = true;
    _manualPhaseCentreRA = ra;
    _manualPhaseCentreDec = dec;
  }
  void SetRFIDetection(bool detectRFI) { _rfiDetection = detectRFI; }
  void SetCollectStatistics(bool collectStatistics) {
    _collectStatistics = collectStatistics;
  }
  void SetUseDysco(bool useDysco) { _useDysco = useDysco; }
  void SetAdvancedDyscoOptions(size_t dataBitRate, size_t weightBitRate,
                               const std::string& distribution,
                               double distTruncation,
                               const std::string& normalization) {
    _dyscoDataBitRate = dataBitRate;
    _dyscoWeightBitRate = weightBitRate;
    _dyscoDistribution = distribution;
    _dyscoDistTruncation = distTruncation;
    _dyscoNormalization = normalization;
  }
  void SetMode(AartfaacMode mode) { _mode = mode; }

 private:
  void allocateBuffers();
  void processAndWriteTimestep(size_t timeIndex, size_t chunkStart,
                               std::vector<std::complex<float>>& vis);
  void initializeWriter(const char* outputFilename);
  void initializeWeights(float* outputWeights, double integrationTime);
  void readAntennaPositions(const char* antennaConfFilename);
  void processBaseline(size_t baseline, aoflagger::Strategy& threadStrategy,
                       aoflagger::QualityStatistics& threadStatistics);
  void writeAartfaacFieldsToMS(const std::string& outputFilename,
                               size_t flagWindowSize);

  void setAntennas();
  void setSPWs();
  void setSource();
  void setField();
  void setObservation();

  size_t NTimestepsSelected() const {
    size_t nTimesteps = _reader->NTimesteps();
    if (_intervalEnd != 0 && nTimesteps > (_intervalEnd - _intervalStart))
      nTimesteps = _intervalEnd - _intervalStart;
    return nTimesteps;
  }

  std::unique_ptr<class AartfaacFile> _reader;
  aoflagger::AOFlagger _flagger;
  std::unique_ptr<aoflagger::QualityStatistics> _statistics;
  std::unique_ptr<Writer> _writer;
  std::string _strategyFile;
  std::mutex _mutex;

  // settings
  AartfaacMode _mode;
  OutputFormat _outputFormat;
  bool _rfiDetection, _collectStatistics, _collectHistograms;
  size_t _timeAvgFactor, _freqAvgFactor;
  double _memPercentage;
  size_t _intervalStart, _intervalEnd;
  bool _manualPhaseCentre;
  double _manualPhaseCentreRA, _manualPhaseCentreDec;
  bool _useDysco;
  size_t _dyscoDataBitRate;
  size_t _dyscoWeightBitRate;
  std::string _dyscoDistribution;
  std::string _dyscoNormalization;
  double _dyscoDistTruncation;
  size_t _threadCount;

  // data fields
  size_t _nParts;
  std::vector<aoflagger::FlagMask> _flagBuffers;
  aoflagger::FlagMask _correlatorMask;
  std::vector<double> _timestepsStart, _timestepsEnd;
  std::vector<UVW> _uvws;
  std::vector<casacore::MPosition> _antennaPositions;
  std::array<double, 9> _antennaAxes;
  casacore::MDirection _phaseDirection;
  std::vector<double> _channelFrequenciesHz;
  // Stores visibility data for multiple timesteps.
  std::vector<std::vector<std::complex<float>>> _visibilityDataBuffer;
  // Duplicate data, but will only be used for statistics
  // collection or rfidetection
  std::vector<aoflagger::ImageSet> _imageSetBuffers;

  // write buffers
  std::vector<bool> _outputFlags;
  aligned_ptr<std::complex<float>> _outputData;
  aligned_ptr<float> _outputWeights;

  Stopwatch _readWatch, _processWatch, _writeWatch;
};

#endif
