#include <cstddef>
#include <iostream>

#include <aoflagger.h>

#include "aartfaac2ms.h"
#include "common/aartfaac/aartfaacmode.h"
#include "common/fixed_tokens_value.h"
#include "parameters_af2ms.h"
#include "units/radeccoord.h"
#include "version.h"

namespace po = boost::program_options;

po::positional_options_description Aartfaac2msParameters::getPositional() {
  po::positional_options_description p;
  p.add("input", 1);
  p.add("output", 1);
  p.add("antennaConfig", 1);
  return p;
}

po::options_description Aartfaac2msParameters::getDescription() {
  po::options_description description("Aartfaac2ms");
  description.add_options()(
      "mem,m", po::value<double>(&_settings.memPercentage)->default_value(50),
      "Max allowed memory usage (percent)")(
      "mode,a", po::value<int>(&_arguments.mode)->default_value(0),
      "Set RCU mode (1-4: LBA, 5-7: HBA).")(
      "interval,i",
      fixed_tokens_value<std::vector<size_t>>(&_arguments.interval, 2, 2),
      "Only convert the selected range of timesteps.")(
      "flag,f", po::value<bool>(&_settings.rfiDetection)->default_value(false),
      "Turn RFI detection on/off. Default is currently off, but this might "
      "change.")(
      "statistics,s",
      po::value<bool>(&_settings.collectStatistics)->default_value(false),
      "Turn statistics collection on/off, default is off.")(
      "centre,c",
      fixed_tokens_value<std::vector<std::string>>(&_arguments.phaseCentreRaDec,
                                                   2, 2),
      "Set alternative phase centre, e.g. -centre 00h00m00.0s 00d00m00.0s.")(
      "dysco,d", po::value<bool>(&_settings.useDysco)->default_value(false),
      "Compress the measurement set with Dysco, using default settings (unless "
      "specified with -dysco-config).")(
      "dysco-config,dc",
      fixed_tokens_value<std::vector<std::string>>(&_arguments.dyscoConfig, 5,
                                                   5),
      "Override default dysco settings.")(
      "cpu,t",
      po::value<size_t>(&_settings.threadCount)
          ->default_value(_SC_NPROCESSORS_ONLN),
      "Number of threads to use.")("version,v", "Print version info and exit.")(
      "input", po::value<std::string>(&_settings.inputFilename)->required(),
      "Input filename")(
      "output", po::value<std::string>(&_settings.outputFilename)->required(),
      "Output filename")(
      "antennaConfig",
      po::value<std::string>(&_settings.antennaConfFilename)->required(),
      "Antenna config filename.");

  return description;
}

void Aartfaac2msParameters::parseArgs(const po::variables_map& vm) {
  // Fill Aartfaac2ms object with non-standard arguments.
  if (vm.count("interval")) {
    if (_arguments.interval.size() != 2) {
      std::cout << "Interval must be of the following format:\n"
                   "--interval <min> <max>"
                << std::endl;
      exit(1);
    }
    _settings.interval = {_arguments.interval[0], _arguments.interval[1]};
  }
  if (vm.count("centre")) {
    if (_arguments.phaseCentreRaDec.size() != 2) {
      std::cout << "Centre must be of the following format:\n"
                   "--centre <min> <max>"
                << std::endl;
      exit(1);
    }
    _settings.phaseCentreRaDec = {
        double(RaDecCoord::ParseRA(_arguments.phaseCentreRaDec[0])),
        double(RaDecCoord::ParseDec(_arguments.phaseCentreRaDec[1]))};
  }
  if (vm.count("dysco-config")) {
    std::string dyscoError =
        "Dysco config must be of the following format:\n "
        "--dysco-config <data bits:int> <weight bits:int> "
        "<distribution:string> "
        "<truncation:double> <normalization:string>";
    if (_arguments.dyscoConfig.size() != 5) {
      std::cout << dyscoError << std::endl;
      exit(1);
    }
    try {
      _settings.dysco = {
          static_cast<size_t>(stoi(_arguments.dyscoConfig[0])),  // dataBitrate
          static_cast<size_t>(
              stoi(_arguments.dyscoConfig[1])),  // weightBitrate
          _arguments.dyscoConfig[2],             // distribution
          stof(_arguments.dyscoConfig[3]),       // distTruncation
          _arguments.dyscoConfig[4]};            // normalization
    } catch (std::exception& e) {
      std::cout << "Could not convert one or more arguments." << std::endl;
      std::cout << dyscoError << std::endl;
      exit(1);
    }
  }
  if (vm.count("version")) {
    std::cout << "Running Aartfaac preprocessing pipeline, version "
              << AF2MS_VERSION_STR << " (" << AF2MS_VERSION_DATE
              << ").\n"
                 "Flagging is performed using AOFlagger "
              << aoflagger::AOFlagger::GetVersionString() << " ("
              << aoflagger::AOFlagger::GetVersionDate() << ").\n";
  }

  // AartfaacMode is always set to an integer.
  _settings.mode = AartfaacMode::FromNumber(_arguments.mode);
}

void Aartfaac2msParameters::configure(Aartfaac2ms& af2ms,
                                      po::variables_map vm) const {
  if (vm.count("mem")) af2ms.SetMemPercentage(_settings.memPercentage);
  if (vm.count("cpu")) af2ms.SetThreadCount(_settings.threadCount);
  if (vm.count("centre"))
    af2ms.SetPhaseCentre(_settings.phaseCentreRaDec.first,
                         _settings.phaseCentreRaDec.second);
  if (vm.count("interval"))
    af2ms.SetInterval(_settings.interval.first, _settings.interval.second);
  if (vm.count("disco-config"))
    af2ms.SetAdvancedDyscoOptions(
        _settings.dysco.dataBitrate, _settings.dysco.weightBitrate,
        _settings.dysco.distribution, _settings.dysco.distTruncation,
        _settings.dysco.normalization);
  if (vm.count("statistics"))
    af2ms.SetCollectStatistics(_settings.collectStatistics);
  if (vm.count("flag")) af2ms.SetRFIDetection(_settings.rfiDetection);
  if (vm.count("dysco")) af2ms.SetUseDysco(_settings.useDysco);
  if (vm.count("mode")) af2ms.SetMode(_settings.mode);
}
