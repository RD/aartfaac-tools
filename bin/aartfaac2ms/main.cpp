#include "units/radeccoord.h"
#include "common/parameter_provider.h"

#include "lib/aartfaac2ms/aartfaac2ms.h"
#include "lib/aartfaac2ms/parameters_af2ms.h"

int main(int argc, char* argv[]) {
  ParameterProvider pp_base(
      "Default usage:\n\t./aartfaac2ms <input> <output> "
      "<antennaConfig>\n\nOptions");
  Aartfaac2msParameters ppa_af2ms;
  pp_base.addArguments(ppa_af2ms);
  pp_base.parse(argc, argv);

  Aartfaac2ms af2ms;
  ppa_af2ms.configure(af2ms, pp_base.getVariableMap());

  std::string& inputFilename = ppa_af2ms.getInputFilename();
  std::string& outputFilename = ppa_af2ms.getOutputFilename();
  std::string& antennaConfFilename = ppa_af2ms.getAntennaConfFilename();

  std::cout << "Reading from " << inputFilename << " and writing to "
            << outputFilename << std::endl;

  af2ms.Run(inputFilename.c_str(), outputFilename.c_str(),
            antennaConfFilename.c_str());

  // Early termination saves a non-neglible amount of time due to not cleaning
  // up the af2ms object, more specifically not cleaning up the vector of
  // ImageSet objects. When processing two timesteps of data, the runtime is
  // reduced from ~1.45 seconds to ~1.2 seconds.
  exit(0);

  return 0;
}
