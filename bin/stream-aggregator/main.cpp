#include "common/aartfaac/aartfaacmode.h"
#include "lib/aartfaac2ms/parameters_af2ms.h"
#include "lib/stream-aggregator/stream_aggregator.h"
#include "lib/stream-aggregator/parameters_stream.h"
#include "common/parameter_provider.h"

int main(int argc, char* argv[]) {
  /*
   * Parse arguments
   */
  ParameterProvider pp("Default usage? I dont know man, it's not done yet.");

  // Add aartfaac2ms arguments.
  Aartfaac2msParameters appa;
  pp.addArguments(appa);

  // Add stream arguments.
  StreamParameters sppa;
  pp.addArguments(sppa);

  // Parse arguments.
  pp.parse(argc, argv);

  /*
   * Initialize stream aggregator
   */
  StreamAggregator sa;

  sppa.configure(sa);

  // Default values.
  sa.setStreams(appa.getInputFilename());
  sa.setMode(AartfaacMode::FromNumber(1));

  sa.run();
  return 0;
}
