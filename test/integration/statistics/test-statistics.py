import unittest
import subprocess
import os
import pandas as pd

# Directories
DATA_DIR = os.environ["DATA_DIR"]
WORK_DIR = os.environ["WORK_DIR"]
VIS_NAME = os.environ["VIS_NAME"]
ANTENNA_NAME = os.environ["ANTENNA_NAME"]
DF_NAME = os.environ["DF_NAME"]
AARTFAAC2MS_BIN = os.environ["AARTFAAC2MS_BIN"]
AOQUALITY_BIN = os.environ["AOQUALITY_BIN"]

# Test and configuration files
VIS_FILE = os.path.join(DATA_DIR, VIS_NAME)
ANTENNA_FILE = os.path.join(DATA_DIR, ANTENNA_NAME)
REF_DF = os.path.join(DATA_DIR, DF_NAME)
TEMP_MS = os.path.join(WORK_DIR, "temp.ms")
TEMP_DF = os.path.join(WORK_DIR, "temp.df")

class IntegrationTests(unittest.TestCase):
    def test_statistics(self):
        os.makedirs(name=WORK_DIR, exist_ok=True)

        # Run the compiled aartfaac2ms binary on the test data.
        cmd = [f"{AARTFAAC2MS_BIN}",
               "--cpu", "4",
               "--mode", "1",
               "--statistics", "on",
               f"{VIS_FILE}",
               f"{TEMP_MS}",
               f"{ANTENNA_FILE}"]
        data = subprocess.run(cmd)
        assert (data.returncode == 0), " ".join(cmd)

        # Check whether aoquality still runs.
        cmd = [f"{AOQUALITY_BIN}",
                "query_b",
                "StandardDeviation",
                f"{TEMP_MS}"]
        null = open(os.devnull, 'w')
        out_file = open(TEMP_DF, 'w')
        data = subprocess.run(cmd, stdout=out_file)
        assert (data.returncode == 0), " ".join(cmd)

        # Open dataframes
        df_ref = pd.read_table(REF_DF)
        df_temp = pd.read_table(TEMP_DF)

        # Compare dataframes
        pd.testing.assert_frame_equal(df_ref, df_temp)

if __name__ == '__main__':
    unittest.main()
