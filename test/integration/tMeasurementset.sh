#!/bin/bash

ORIG_DIR=$(pwd)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Set some environment variables
export DATA_DIR=${DIR}/tmp/data
export WORK_DIR=${DIR}/tmp/workdir
export VIS_NAME="a12-212-1sec-upgraded.vis"
export ANTENNA_NAME="A12-AntennaField.conf"
export MSREF_NAME="a12-SB212-1sec-reference.ms"
export AARTFAAC2MS_BIN=$(which aartfaac2ms)

# Check whether binaries are available
if [ -z ${AARTFAAC2MS_BIN} ]
then
    echo "Error: 'aartfaac2ms' binary not found!"
    exit 1
fi

# Download test input data
for FILENAME in ${VIS_NAME}.zip ${ANTENNA_NAME} ${MSREF_NAME}.zip
do
    ${DIR}/../../scripts/download-resource.sh ${FILENAME}
done

# Create a working directory
mkdir -p $WORK_DIR
cd $WORK_DIR

# Run the test
pytest -s -v --exitfirst ${DIR}/measurementset/test-measurementset.py
