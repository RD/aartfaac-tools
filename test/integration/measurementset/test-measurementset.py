import unittest
import subprocess
import os
import casacore.tables
import numpy as np

# Directories
DATA_DIR = os.environ["DATA_DIR"]
WORK_DIR = os.environ["WORK_DIR"]
VIS_NAME = os.environ["VIS_NAME"]
ANTENNA_NAME = os.environ["ANTENNA_NAME"]
MSREF_NAME = os.environ["MSREF_NAME"]
AARTFAAC2MS_BIN = os.environ["AARTFAAC2MS_BIN"]

# Test and configuration files
VIS_FILE = os.path.join(DATA_DIR, VIS_NAME)
ANTENNA_FILE = os.path.join(DATA_DIR, ANTENNA_NAME)
REF_MS = os.path.join(DATA_DIR, MSREF_NAME)
TEMP_MS = os.path.join(WORK_DIR, "temp.ms")

class IntegrationTests(unittest.TestCase):
    def test_quality(self):
        os.makedirs(name=WORK_DIR, exist_ok=True)

        # Run the compiled aartfaac2ms binary on the test data.
        cmd = [f"{AARTFAAC2MS_BIN}",
               "--cpu", "4",
               "--mode", "1",
               "--interval", "0", "1",
               f"{VIS_FILE}",
               f"{TEMP_MS}",
               f"{ANTENNA_FILE}"]
        data = subprocess.run(cmd)
        assert (data.returncode == 0), " ".join(cmd)

        # Open measurementsets
        t_ref = casacore.tables.table(f"{REF_MS}")
        t_temp = casacore.tables.table(f"{TEMP_MS}")

        # The measurementsets should have the same columns
        assert (t_ref.colnames() == t_temp.colnames())

        # Check a few columns
        for columnname in t_ref.colnames():
            print(f"Checking column '{columnname}'")

            # Read data columns
            col_ref = t_ref.getcol(columnname)
            col_temp = t_temp.getcol(columnname)

            # Compare data columns
            np.testing.assert_allclose(col_ref, col_temp, rtol=1.e-6)


if __name__ == '__main__':
    unittest.main()
